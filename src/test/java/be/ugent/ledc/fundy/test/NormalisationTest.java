package be.ugent.ledc.fundy.test;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.binding.jdbc.agents.PersistentDatatype;
import be.ugent.ledc.core.binding.jdbc.schema.Key;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.normalization.BCNFDecomposition;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.NormalisationException;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.io.FDParser;
import java.util.Map;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

public class NormalisationTest
{

    @Test
    public void bcnfDecompositionTest1() throws NormalisationException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("name"), "id"),
            new SimpleFD(SetOperations.set("id"), "name"),
            new SimpleFD(SetOperations.set("id","teacher"), "type"),
            new SimpleFD(SetOperations.set("name","teacher"), "type")
        );
        
        Set<String> attributes = SetOperations.set("teacher", "name", "id", "type");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("courses");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        Map<TableSchema, RuleSet<Dataset, FD>> decomposition = new BCNFDecomposition()
            .decompose(tableSchema, ruleset);
        
        TableSchema left = new TableSchema();
        left.setAttributeProperty("name", TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        left.setAttributeProperty("id", TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        left.setName("courses_1");
        left.setPrimaryKey(new Key(SetOperations.set("name")));
        
        TableSchema right = new TableSchema();
        right.setAttributeProperty("name", TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        right.setAttributeProperty("teacher", TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        right.setAttributeProperty("type", TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        right.setName("courses_2");
        right.setPrimaryKey(new Key(SetOperations.set("name", "teacher")));
        
        assertTrue(decomposition.get(left).size() == 2);
        assertTrue(decomposition.get(right).size() == 1);
    }
    
    @Test
    public void bcnfDecompositionTest2() throws NormalisationException, ParseException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseFD("a -> b"),
            FDParser.parseFD("b,c -> d"),
            FDParser.parseFD("e -> f,g"),
            FDParser.parseFD("g -> f"),
            FDParser.parseFD("e,f,g -> d")
        );
        
        Set<String> attributes = SetOperations.set("a", "b", "c", "d", "e", "f", "g");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        Map<TableSchema, RuleSet<Dataset, FD>> decomposition = new BCNFDecomposition()
            .decompose(tableSchema, ruleset);
        
        assertTrue(decomposition
            .keySet()
            .stream()
            .map(ts -> ts.getAttributeNames())
            .noneMatch(attr -> (attr.contains("a") || attr.contains("c")) && attr.contains("f"))
        );
    }
}
