package be.ugent.ledc.fundy.test;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.core.util.ListOperations;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.algorithms.reasoning.MinimalCoverGenerator;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import be.ugent.ledc.fundy.io.FDParser;
import java.util.HashSet;
import org.junit.Assert;
import org.junit.Test;


public class MinimalCoverTest
{
    @Test
    public void minimalCoverTest1()
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new FD(SetOperations.set("A"), SetOperations.set("B", "C", "D", "E", "F")),
            new FD(SetOperations.set("B", "C"), SetOperations.set("A", "D", "E", "F"))
        );
        
        RuleSet<Dataset, SimpleFD> result = new MinimalCoverGenerator().generate(ruleset);
        
        RuleSet<Dataset, SimpleFD> expected = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("A"), "C"),
            new SimpleFD(SetOperations.set("A"), "D"),
            new SimpleFD(SetOperations.set("A"), "E"),
            new SimpleFD(SetOperations.set("A"), "F"),
            new SimpleFD(SetOperations.set("B", "C"), "A")
        );
        
        Assert.assertEquals(expected, result);
    }
    
    @Test
    public void minimalCoverTest2()
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("B"), "C"),
            new SimpleFD(SetOperations.set("A"), "C")
        );
        
        RuleSet<Dataset, SimpleFD> result = new MinimalCoverGenerator().generate(ruleset);
        
        RuleSet<Dataset, SimpleFD> expected = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("B"), "C")
        );
        
        Assert.assertEquals(expected, result);
    }
    
    @Test
    public void minimalCoverTest3()
    {
        
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new FD(SetOperations.set("A"), SetOperations.set("B", "C")),
            new SimpleFD(SetOperations.set("B"), "C"),
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("A", "B"), "C")
        );
        
        RuleSet<Dataset, SimpleFD> result = new MinimalCoverGenerator().generate(ruleset);
        
        RuleSet<Dataset, SimpleFD> expected = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("B"), "C")
        );
        
        Assert.assertEquals(expected, result);
    }
    
    @Test
    public void minimalCoverTest4() throws ParseException
    {
        
        RuleSet<Dataset, FD> ruleset = FDParser.parseRuleset(
            ListOperations.list(
                "a -> b,a",
                "b -> b",
                " -> c",
                "a,c -> b"
            ));
        
        RuleSet<Dataset, SimpleFD> result = new MinimalCoverGenerator().generate(ruleset);
        
        RuleSet<Dataset, SimpleFD> expected = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("a"), "b"),
            new SimpleFD(new HashSet<>(), "c")
        );
        
        Assert.assertEquals(expected, result);
    }
}
