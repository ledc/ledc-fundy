package be.ugent.ledc.fundy.test;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.algorithms.discovery.tane.TaneException;
import be.ugent.ledc.fundy.algorithms.discovery.tane.TaneMiner;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.io.FDParser;
import java.util.stream.Collectors;
import org.junit.Assert;
import org.junit.Test;

public class TaneTest
{
    @Test
    public void taneExactTest() throws ParseException, TaneException
    {      
        SimpleDataset data = new SimpleDataset();
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", "a")
            .set("c", "$")
            .set("d", "Flower")
        );
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", "A")
            .set("c", "*")
            .set("d", "Tulip")
        );
        
        data.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", "A")
            .set("c", "$")
            .set("d", "Daffodil")
        );
        
        data.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", "A")
            .set("c", "$")
            .set("d", "Flower")
        );
        
        data.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", "b")
            .set("c", "*")
            .set("d", "Lilly")
        );
        
        data.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", "b")
            .set("c", "$")
            .set("d", "Orchid")
        );
        
        data.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", "C")
            .set("c", "*")
            .set("d", "Flower")
        );
        
        data.addDataObject(new DataObject()
            .set("a", 3)
            .set("b", "C")
            .set("c", "#")
            .set("d", "Rose")
        );
        
        RuleSet<Dataset, FD> expected = new RuleSet<>
        (
            FDParser.parseFD("a,d->b"),
            FDParser.parseFD("a,d->c"),
            FDParser.parseFD("b,d->a"),
            FDParser.parseFD("b,d->c"),            
            FDParser.parseFD("b,c->a"),
            FDParser.parseFD("a,c->b")
        );
        
        RuleSet<Dataset, FD> found =
            new RuleSet<>(
                new TaneMiner(SetOperations.set("a","b","c","d"))
                .discover(data)
                .stream()
                .map(sfd -> new FD(sfd.getLeftHandSide(),sfd.getRightHandSide()))
                .collect(Collectors.toSet())
            );
        
        Assert.assertEquals(expected, found);

    }
    
    @Test
    public void taneApproximateTest() throws ParseException, TaneException
    {      
        SimpleDataset data = new SimpleDataset();
        
        for(int i=0; i<99; i++)
        {
            data.addDataObject(new DataObject().set("a", i).set("b", "a"));
        }
        
        data.addDataObject(new DataObject().set("a", 0).set("b", "b"));

        RuleSet<Dataset, FD> expected = new RuleSet<>(FDParser.parseFD("a->b"));
        
        RuleSet<Dataset, FD> found =
            new RuleSet<>(
                new TaneMiner(SetOperations.set("a","b"), 0.01)
                .discover(data)
                .stream()
                .map(sfd -> new FD(sfd.getLeftHandSide(),sfd.getRightHandSide()))
                .collect(Collectors.toSet())
            );
        
        Assert.assertEquals(expected, found);

    }
}
