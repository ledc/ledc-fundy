package be.ugent.ledc.fundy.test;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import be.ugent.ledc.fundy.io.FDParser;
import java.util.HashSet;
import static org.junit.Assert.assertEquals;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ParseTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    @Test
    public void parseTest1() throws ParseException
    {
        SimpleFD fd = FDParser.parseSimpleFD("b->c");
        
        assertEquals(new SimpleFD(SetOperations.set("b"), "c"), fd);
    }
    
    @Test
    public void parseTest2() throws ParseException
    {
        SimpleFD fd = FDParser.parseSimpleFD("a,b->c");
        
        assertEquals(new SimpleFD(SetOperations.set("a", "b"), "c"), fd);
    }
    
    @Test
    public void parseTest3() throws ParseException
    {
        FD fd = FDParser.parseFD("a,b->c,d");
        
        assertEquals(new FD(SetOperations.set("a", "b"), SetOperations.set("c","d")), fd);
    }
    
    @Test
    public void parseTest4() throws ParseException
    {
        FD fd = FDParser.parseFD("a,b->a");
        
        assertEquals(new FD(SetOperations.set("a", "b"), SetOperations.set("a")), fd);
    }
    
    @Test
    public void parseTest5() throws ParseException
    {
        SimpleFD fd = FDParser.parseSimpleFD("->a");
        
        assertEquals(new SimpleFD(new HashSet<>(), "a"), fd);
    }
    
    @Test
    public void parseTest6() throws ParseException
    {
        exceptionRule.expect(ParseException.class);
        exceptionRule.expectMessage("Cannot parse FD. Cause: empty attribute detected.");
        FDParser.parseSimpleFD("b,->a");
    }
    
    @Test
    public void parseTest7() throws ParseException
    {
        exceptionRule.expect(ParseException.class);
        exceptionRule.expectMessage("Cannot parse FD. Cause: empty attribute detected.");
        FDParser.parseSimpleFD(",b->a");
    }
    
    @Test
    public void parseTest8() throws ParseException
    {
        exceptionRule.expect(ParseException.class);
        exceptionRule.expectMessage("Cannot parse FD. Cause: empty attribute detected.");
        FDParser.parseSimpleFD("c,,b->a");
    }
}
