package be.ugent.ledc.fundy.test;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.cost.ConstantCostFunction;
import be.ugent.ledc.core.cost.CostFunction;
import be.ugent.ledc.core.cost.string.LevenshteinDistanceCostFunction;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.repair.SwipeR;
import be.ugent.ledc.fundy.algorithms.repair.cost.MaxCostFunction;
import be.ugent.ledc.fundy.algorithms.repair.cost.SwipeCostModel;
import be.ugent.ledc.fundy.algorithms.repair.priority.ReliabilityPriority;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.io.FDParser;
import java.util.HashMap;
import java.util.Map;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class SwipeRTest
{
    @Test
    public void lhsRegexRepairTest() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a -> b")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject().set("a", "aaa").set("b", 0));
        dirty.addDataObject(new DataObject().set("a", "aaa").set("b", 1));
        dirty.addDataObject(new DataObject().set("a", "aaa2").set("b", 4));
        dirty.addDataObject(new DataObject().set("a", "aaa").set("b", 3));
        
        dirty.addDataObject(new DataObject().set("a", "bbb").set("b", 0));
        dirty.addDataObject(new DataObject().set("a", "bbb8").set("b", 2));

        Map<String,CostFunction<?>> cfm = new HashMap<>();
        cfm.put("a", new LevenshteinDistanceCostFunction());
        cfm.put("b", new MaxCostFunction());

        SwipeCostModel cm = new SwipeCostModel(cfm);        

        Map<String,String> regexes = new HashMap<>();
        regexes.put("a", "[a-z]{3}");
        
        SwipeR swiper = new SwipeR(
            ruleset,
            cm,
            regexes
        );
        
        Dataset repair = swiper.repair(dirty);
        
        Dataset expected = new SimpleDataset();
        
        expected.addDataObject(new DataObject().set("a", "aaa").set("b", 4));
        expected.addDataObject(new DataObject().set("a", "aaa").set("b", 4));
        expected.addDataObject(new DataObject().set("a", "aaa").set("b", 4));
        expected.addDataObject(new DataObject().set("a", "aaa").set("b", 4));
        
        expected.addDataObject(new DataObject().set("a", "bbb").set("b", 2));
        expected.addDataObject(new DataObject().set("a", "bbb").set("b", 2));
        
        assertEquals(expected, repair);   
    }
    
    @Test
    public void rhsRegexRepairTest() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a -> b")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject().set("a", "aaa").set("b", "0"));
        dirty.addDataObject(new DataObject().set("a", "aaa").set("b", "zz"));
        dirty.addDataObject(new DataObject().set("a", "aaa").set("b", "zz"));
        dirty.addDataObject(new DataObject().set("a", "aaa").set("b", "zz"));
        
        dirty.addDataObject(new DataObject().set("a", "bbb").set("b", "yy"));
        dirty.addDataObject(new DataObject().set("a", "bbb").set("b", "xx"));

        Map<String,CostFunction<?>> cfm = new HashMap<>();
        cfm.put("b", new MaxCostFunction());

        SwipeCostModel cm = new SwipeCostModel(cfm);        

        Map<String,String> regexes = new HashMap<>();
        regexes.put("b", "\\d+");
        
        SwipeR swiper = new SwipeR(
            ruleset,
            cm,
            regexes
        );
        
        Dataset repair = swiper.repair(dirty);
        
        Dataset expected = new SimpleDataset();
        
        expected.addDataObject(new DataObject().set("a", "aaa").set("b", "0"));
        expected.addDataObject(new DataObject().set("a", "aaa").set("b", "0"));
        expected.addDataObject(new DataObject().set("a", "aaa").set("b", "0"));
        expected.addDataObject(new DataObject().set("a", "aaa").set("b", "0"));
        
        expected.addDataObject(new DataObject().set("a", "bbb").set("b", null));
        expected.addDataObject(new DataObject().set("a", "bbb").set("b", null));
        
        assertEquals(expected, repair);   
    }
    
    @Test
    public void rhsRegexRepairTest2() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a -> b")
        );
        
        Dataset dirty = new SimpleDataset();
        
        //No FD violation, but the RHS do not match the regex
        dirty.addDataObject(new DataObject().set("a", "aaa").set("b", "zz"));
        dirty.addDataObject(new DataObject().set("a", "aaa").set("b", "zz"));

        Map<String,CostFunction<?>> cfm = new HashMap<>();
        cfm.put("b", new ConstantCostFunction<>());

        SwipeCostModel cm = new SwipeCostModel(cfm);        

        Map<String,String> regexes = new HashMap<>();
        regexes.put("b", "\\d+");
        
        SwipeR swiper = new SwipeR(
            ruleset,
            cm,
            regexes
        );
        
        Dataset repair = swiper.repair(dirty);
        
        Dataset expected = new SimpleDataset();

        expected.addDataObject(new DataObject().set("a", "aaa").set("b", null));
        expected.addDataObject(new DataObject().set("a", "aaa").set("b", null));
        
        assertEquals(expected, repair);   
    }
    
    @Test
    public void entropyTest() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a -> b")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject().set("a", "x").set("b", "1"));
        dirty.addDataObject(new DataObject().set("a", "x").set("b", "2"));
        dirty.addDataObject(new DataObject().set("a", "x").set("b", "3"));
        dirty.addDataObject(new DataObject().set("a", "x").set("b", "4"));
        
        dirty.addDataObject(new DataObject().set("a", "y").set("b", "1"));
        dirty.addDataObject(new DataObject().set("a", "y").set("b", "1"));
        dirty.addDataObject(new DataObject().set("a", "y").set("b", "1"));
        dirty.addDataObject(new DataObject().set("a", "y").set("b", "4"));

        Map<String,CostFunction<?>> cfm = new HashMap<>();
        cfm.put("b", new ConstantCostFunction<>());

        SwipeCostModel cm = new SwipeCostModel(cfm);        

        Map<String,String> regexes = new HashMap<>();
        
        SwipeR swiper = new SwipeR(
            ruleset,
            cm,
            new ReliabilityPriority(),
            regexes,
            true,
            1.5 //Entropy cannot be higher than 1.5
        );
        
        Dataset repair = swiper.repair(dirty);
        
        Dataset expected = new SimpleDataset();

        expected.addDataObject(new DataObject().set("a", null).set("b", "1"));
        expected.addDataObject(new DataObject().set("a", null).set("b", "2"));
        expected.addDataObject(new DataObject().set("a", null).set("b", "3"));
        expected.addDataObject(new DataObject().set("a", null).set("b", "4"));
        
        expected.addDataObject(new DataObject().set("a", "y").set("b", "1"));
        expected.addDataObject(new DataObject().set("a", "y").set("b", "1"));
        expected.addDataObject(new DataObject().set("a", "y").set("b", "1"));
        expected.addDataObject(new DataObject().set("a", "y").set("b", "1"));
        
        assertEquals(expected, repair);
    }
}
