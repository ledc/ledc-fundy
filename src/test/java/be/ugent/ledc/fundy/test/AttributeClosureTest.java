package be.ugent.ledc.fundy.test;

import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.algorithms.reasoning.AttributeClosureGenerator;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class AttributeClosureTest
{
    private static RuleSet<Dataset, FD> ruleset1;
    
    private static RuleSet<Dataset, FD> ruleset2;
    
    private static RuleSet<Dataset, FD> ruleset3;
    
    @BeforeClass
    public static void init()
    {
        
        ruleset1 = new RuleSet<>
        (
            new SimpleFD(Stream.of("C").collect(Collectors.toSet()), "T"),
            new SimpleFD(Stream.of("H", "R").collect(Collectors.toSet()), "C"),
            new SimpleFD(Stream.of("H", "T").collect(Collectors.toSet()), "R"),
            new SimpleFD(Stream.of("C", "S").collect(Collectors.toSet()), "G"),
            new SimpleFD(Stream.of("H", "S").collect(Collectors.toSet()), "R")
        );
        
        ruleset2 = new RuleSet<>
        (
            new SimpleFD(Stream.of("A", "D").collect(Collectors.toSet()), "B"),
            new SimpleFD(Stream.of("A", "B").collect(Collectors.toSet()), "E"),
            new SimpleFD(Stream.of("C").collect(Collectors.toSet()), "D"),
            new SimpleFD(Stream.of("B").collect(Collectors.toSet()), "C"),
            new SimpleFD(Stream.of("A", "C").collect(Collectors.toSet()), "F")
        );
        
        ruleset3 = new RuleSet<>
        (
            new SimpleFD(Stream.of("A").collect(Collectors.toSet()), "B"),
            new SimpleFD(Stream.of("B").collect(Collectors.toSet()), "C"),
            new SimpleFD(new HashSet<>(), "D")
        );
    }
    
    @Test
    public void closureTest1()
    {
        AttributeClosureGenerator acg = new AttributeClosureGenerator(ruleset1);
        
        Set<String> result = acg.generate(Stream
            .of("H", "S")
            .collect(Collectors.toSet()));
        
        Set<String> expected = Stream
            .of("H", "S", "R", "T", "C", "G")
            .collect(Collectors.toSet());
        
        Assert.assertEquals(expected, result);
    }
    
    @Test
    public void closureTest2()
    {
        AttributeClosureGenerator acg = new AttributeClosureGenerator(ruleset1);
        
        Set<String> result = acg.generate(Stream
            .of("C", "S")
            .collect(Collectors.toSet()));
        
        Set<String> expected = Stream
            .of("C", "S", "G", "T")
            .collect(Collectors.toSet());
        
        Assert.assertEquals(expected, result);
    }
    
    @Test
    public void closureTest3()
    {
        AttributeClosureGenerator acg = new AttributeClosureGenerator(ruleset2);
        
        Set<String> result = acg.generate(Stream
            .of("A")
            .collect(Collectors.toSet()));
        
        Set<String> expected = Stream
            .of("A")
            .collect(Collectors.toSet());
        
        Assert.assertEquals(expected, result);
    }
    
    @Test
    public void closureTest4()
    {
        AttributeClosureGenerator acg = new AttributeClosureGenerator(ruleset2);
        
        Set<String> result = acg.generate(Stream
            .of("B")
            .collect(Collectors.toSet()));
        
        Set<String> expected = Stream
            .of("B", "C", "D")
            .collect(Collectors.toSet());
        
        Assert.assertEquals(expected, result);
    }
    
    @Test
    public void closureTest5()
    {
        AttributeClosureGenerator acg = new AttributeClosureGenerator(ruleset2);
        
        Set<String> result = acg.generate(Stream
            .of("A", "B")
            .collect(Collectors.toSet()));
        
        Set<String> expected = Stream
            .of("A", "B", "C", "D", "E", "F")
            .collect(Collectors.toSet());
        
        Assert.assertEquals(expected, result);
    }
    
    @Test
    public void closureTest6()
    {
        AttributeClosureGenerator acg = new AttributeClosureGenerator(ruleset3);
        
        Set<String> result = acg.generate(SetOperations.set("A"));
        
        Set<String> expected = SetOperations.set("A", "B", "C", "D");
        
        Assert.assertEquals(expected, result);
    }

}
