package be.ugent.ledc.fundy.test;

import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.algorithms.reasoning.ImplicationVerifier;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import org.junit.Test;
import static org.junit.Assert.*;

public class ImplicationTest
{
    
    @Test
    public void implicationTest1()
    {
        assertTrue(ImplicationVerifier.isImplied(new SimpleFD(SetOperations.set("A", "B"), "F"),
            new RuleSet<>
            (
                new SimpleFD(SetOperations.set("A", "B"), "C"),
                new SimpleFD(SetOperations.set("A"), "D"),
                new FD(SetOperations.set("C", "D"), SetOperations.set("E", "F"))
            )
        ));
    }
}
