package be.ugent.ledc.fundy.test;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.cost.ConstantCostFunction;
import be.ugent.ledc.core.cost.CostFunction;
import be.ugent.ledc.core.cost.WeightedCostFunction;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.core.util.ListOperations;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.algorithms.reasoning.MinimalCoverGenerator;
import be.ugent.ledc.fundy.algorithms.repair.Cell;
import be.ugent.ledc.fundy.algorithms.repair.CellMap;
import be.ugent.ledc.fundy.algorithms.repair.PartitionGenerator;
import be.ugent.ledc.fundy.algorithms.repair.Swipe;
import be.ugent.ledc.fundy.algorithms.repair.cost.SwipeCostModel;
import be.ugent.ledc.fundy.algorithms.repair.SwipePartition;
import be.ugent.ledc.fundy.algorithms.repair.SwipeRepair;
import be.ugent.ledc.fundy.algorithms.repair.cost.MaxCostFunction;
import be.ugent.ledc.fundy.algorithms.repair.cost.MinCostFunction;
import be.ugent.ledc.fundy.algorithms.repair.priority.FixedPriority;
import be.ugent.ledc.fundy.algorithms.repair.priority.ReliabilityPriority;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import be.ugent.ledc.fundy.io.FDParser;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.junit.Assert;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class SwipeTest
{
    @Test
    public void partitionTest1() throws ParseException, RepairException
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("b->c"),
            FDParser.parseSimpleFD("a->c"),
            FDParser.parseSimpleFD("e,c->d"),
            FDParser.parseSimpleFD("d -> c"),
            FDParser.parseSimpleFD("d -> e")
        );
        
        SwipePartition prepar = PartitionGenerator.generate(ruleset);
        
        SwipePartition expected = new SwipePartition();
        expected.addRepairClass(SetOperations.set("a"));
        expected.addRepairClass(SetOperations.set("b"));
        expected.addRepairClass(SetOperations.set("c", "d", "e"));
        
        assertEquals(expected, prepar);
    }
    
    @Test
    public void partitionTest2() throws ParseException, RepairException
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a->b"),
            FDParser.parseSimpleFD("b->c")
        );
        
        SwipePartition prepar = PartitionGenerator.generate(
            ruleset
        );
        
        SwipePartition expected = new SwipePartition();
        expected.addRepairClass(SetOperations.set("a"));
        expected.addRepairClass(SetOperations.set("b"));
        expected.addRepairClass(SetOperations.set("c"));
        
        assertEquals(expected, prepar);
    }
    
    @Test
    public void partitionTest3() throws ParseException, RepairException
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a->b"),
            FDParser.parseSimpleFD("b->c")
        );
        
        SwipePartition prepar = PartitionGenerator.generate(
            ruleset
        );
        
        SwipePartition expected = new SwipePartition();
        expected.addRepairClass(SetOperations.set("a"));
        expected.addRepairClass(SetOperations.set("b"));
        expected.addRepairClass(SetOperations.set("c"));
        
        assertEquals(expected, prepar);
    }
    
    @Test
    public void partitionTest4() throws ParseException, RepairException
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a -> b"),
            FDParser.parseSimpleFD("b,c -> d"),
            FDParser.parseSimpleFD("a,c -> d")
        );
        
        SwipePartition part = PartitionGenerator.generate(ruleset);

        assertTrue(part.classIndexOf("a") < part.classIndexOf("b"));
        assertTrue(part.classIndexOf("b") < part.classIndexOf("d"));
        assertTrue(part.classIndexOf("c") < part.classIndexOf("d"));
        
    }
    
    @Test
    public void partitionTest5() throws ParseException, RepairException
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (               
            FDParser.parseSimpleFD("e->a"),
            FDParser.parseSimpleFD("b,c->a"),
            FDParser.parseSimpleFD("c,e->a"),
            FDParser.parseSimpleFD("d->a"),
            FDParser.parseSimpleFD("a->b"),
            FDParser.parseSimpleFD("a,c->b"),
            FDParser.parseSimpleFD("c->a"),
            FDParser.parseSimpleFD("b,d->a")
        );
        
        SwipePartition part = PartitionGenerator.generate(ruleset);
        
        assertTrue(part.classIndexOf("e") < part.classIndexOf("a"));
        assertTrue(part.classIndexOf("c") < part.classIndexOf("a"));
        assertTrue(part.classIndexOf("d") < part.classIndexOf("a"));
        assertTrue(part.classIndexOf("e") < part.classIndexOf("b"));
        assertTrue(part.classIndexOf("c") < part.classIndexOf("b"));
        assertTrue(part.classIndexOf("d") < part.classIndexOf("b"));
        assertTrue(part.classIndexOf("a") < part.classIndexOf("b"));
        
        //Check that partition is forward repairable
        assertTrue(new MinimalCoverGenerator()
            .generate(ruleset)
            .stream()
            .allMatch(fd -> part.forward(fd))
        );
    }
    
    @Test
    public void partitionTest6() throws ParseException, RepairException
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (                 
            FDParser.parseSimpleFD("c,e->a"),
            FDParser.parseSimpleFD("e,g,l->a"),
            FDParser.parseSimpleFD("a,i,m->b"),
            FDParser.parseSimpleFD("a,f,g,l,m->b"),
            FDParser.parseSimpleFD("f,j->a"),
            FDParser.parseSimpleFD("c,e,f,h,j->a"),
            FDParser.parseSimpleFD("e,n->a"),
            FDParser.parseSimpleFD("a,b,c,g->d"),
            FDParser.parseSimpleFD("f,i,j->a"),
            FDParser.parseSimpleFD("a,e->b")
        );
        
        SwipePartition part = PartitionGenerator.generate(ruleset);

        System.out.println(part);
        
        assertTrue(new MinimalCoverGenerator()
            .generate(ruleset)
            .stream()
            .anyMatch(fd -> part.forward(fd)));

        
    }
    
    @Test
    public void partitionTest7() throws ParseException, RepairException
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (                 
            FDParser.parseSimpleFD("a,b,f,m->c"),
            FDParser.parseSimpleFD("c,h,k,l->a"),
            FDParser.parseSimpleFD("h->a"),
            FDParser.parseSimpleFD("b,l,m->a"),
            FDParser.parseSimpleFD("a,e,h,j->b"),
            FDParser.parseSimpleFD("b,f,i,j->a"),
            FDParser.parseSimpleFD("f,h,k,n->a"),
            FDParser.parseSimpleFD("a,c,g,l->b"),
            FDParser.parseSimpleFD("f,g->a"),
            FDParser.parseSimpleFD("l,m->a")
        );
        
        SwipePartition part = PartitionGenerator.generate(ruleset);
        
        assertTrue(new MinimalCoverGenerator()
            .generate(ruleset)
            .stream()
            .anyMatch(fd -> part.forward(fd)));   
    }
    
    @Test
    public void partitionGinsburgTest() throws ParseException, RepairException
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a->g"),
            FDParser.parseSimpleFD("b->g"),
            FDParser.parseSimpleFD("c->h"),
            FDParser.parseSimpleFD("e->h"),
            FDParser.parseSimpleFD("g,h->f")
        );
        
        SwipePartition part = PartitionGenerator.generate(ruleset);
        
        assertTrue(part.classIndexOf("a") < part.classIndexOf("g"));
        assertTrue(part.classIndexOf("a") < part.classIndexOf("f"));
        assertTrue(part.classIndexOf("b") < part.classIndexOf("g"));
        assertTrue(part.classIndexOf("b") < part.classIndexOf("f"));
        assertTrue(part.classIndexOf("c") < part.classIndexOf("h"));
        assertTrue(part.classIndexOf("c") < part.classIndexOf("f"));
        assertTrue(part.classIndexOf("e") < part.classIndexOf("h"));
        assertTrue(part.classIndexOf("e") < part.classIndexOf("f"));
        assertTrue(part.classIndexOf("g") < part.classIndexOf("f"));
        assertTrue(part.classIndexOf("h") < part.classIndexOf("f"));
    }
    
    @Test
    public void hospitalPartitionTest() throws ParseException, RepairException
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("name->city"),
            FDParser.parseSimpleFD("name->zip"),
            FDParser.parseSimpleFD("name->state"),
            FDParser.parseSimpleFD("name->pnum"),
            FDParser.parseSimpleFD("pnum->name"),
            FDParser.parseSimpleFD("city->county"),
            FDParser.parseSimpleFD("code->condition")
        );

        SwipePartition part = PartitionGenerator.generate(
            ruleset
        );
           
        assertEquals(part.classIndexOf("pnum"), part.classIndexOf("name"));
        assertTrue(part.classIndexOf("name") < part.classIndexOf("city"));
        assertTrue(part.classIndexOf("name") < part.classIndexOf("zip"));
        assertTrue(part.classIndexOf("name") < part.classIndexOf("state"));
        assertTrue(part.classIndexOf("name") < part.classIndexOf("county"));
        assertTrue(part.classIndexOf("city") < part.classIndexOf("county"));
        assertTrue(part.classIndexOf("code") < part.classIndexOf("condition"));
    }
    
    @Test
    public void classIndexTest1() throws ParseException, RepairException
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a->b"),
            FDParser.parseSimpleFD("c->d")
        );
                
        SwipePartition partition = PartitionGenerator.generate(ruleset);
        
        Assert.assertTrue(partition.classIndexOf("a") < partition.classIndexOf("b"));
        Assert.assertTrue(partition.classIndexOf("c") < partition.classIndexOf("d"));
    }
    
    @Test
    public void classIndexTest2() throws ParseException, RepairException
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a->c")
        );
                
        SwipePartition partition = PartitionGenerator.generate(ruleset);
        
        Assert.assertTrue(partition.classIndexOf("a") < partition.classIndexOf("c"));
    }
    
    @Test
    public void classIndexTest3() throws ParseException, RepairException
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a->b"),
            FDParser.parseSimpleFD("b->c"),
            FDParser.parseSimpleFD("c->d"),
            FDParser.parseSimpleFD("d->e")
        );
        
        SwipePartition partition = PartitionGenerator.generate(ruleset);
        
        Assert.assertTrue(partition.classIndexOf("a") < partition.classIndexOf("b"));
        Assert.assertTrue(partition.classIndexOf("b") < partition.classIndexOf("c"));
        Assert.assertTrue(partition.classIndexOf("c") < partition.classIndexOf("d"));
        Assert.assertTrue(partition.classIndexOf("d") < partition.classIndexOf("e"));
    }
    
    @Test
    public void majorityVoteTest1() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseFD("a->c"),
            FDParser.parseFD("b->c")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject().set("a", 0).set("b", 0).set("c", 1));
        dirty.addDataObject(new DataObject().set("a", 0).set("b", 0).set("c", 2));
        dirty.addDataObject(new DataObject().set("a", 0).set("b", 0).set("c", 1));
        
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 3));
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 2));
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 3));
        
        dirty.addDataObject(new DataObject().set("a", 2).set("b", 0).set("c", 4));
        dirty.addDataObject(new DataObject().set("a", 2).set("b", 0).set("c", 2));
        dirty.addDataObject(new DataObject().set("a", 2).set("b", 0).set("c", 4));
        
        SwipeCostModel cm = new SwipeCostModel(ruleset);
        
        Swipe swipe = new Swipe(
            ruleset,
            cm,
            new FixedPriority(ListOperations.list("a", "b", "c")),
            false //No propagation
        );
        
        Dataset repair = swipe.repair(dirty);
        
        assertEquals(1, repair.project("c").distinct().getSize());
        assertEquals(2, repair.project("c").distinct().stream().findFirst().get().get("c"));
        
    }
    
    @Test
    public void majorityVoteTest2() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseFD("a->c"),
            FDParser.parseFD("b->c")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject().set("a", 0).set("b", 0).set("c", 1));
        dirty.addDataObject(new DataObject().set("a", 0).set("b", 1).set("c", 1));
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 1).set("c", 2));
        dirty.addDataObject(new DataObject().set("a", 2).set("b", 1).set("c", 2));
        dirty.addDataObject(new DataObject().set("a", 3).set("b", 1).set("c", 2));

        SwipeCostModel cm = new SwipeCostModel(ruleset);
        
        Swipe swipe = new Swipe(
            ruleset,
            cm,
            new FixedPriority(ListOperations.list("a", "b", "c"))
        );
        
        Dataset repair = swipe.repair(dirty);
        
        assertEquals(1, repair.project("c").distinct().getSize());
        assertEquals(2, repair.project("c").distinct().stream().findFirst().get().get("c"));
        
    }
    
    @Test
    public void foodInspectionTest() throws ParseException, RepairException
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("address->state"),
            FDParser.parseSimpleFD("zip->state"),
            FDParser.parseSimpleFD("license_number->state"),
            FDParser.parseSimpleFD("city->state"),
            FDParser.parseSimpleFD("dba_name->state"),
            FDParser.parseSimpleFD("aka_name->state"),
                
            FDParser.parseSimpleFD("zip,aka_name,dba_name->city"),
            FDParser.parseSimpleFD("address,license_number->city"),
            FDParser.parseSimpleFD("aka_name,license_number->city"),
            FDParser.parseSimpleFD("address,dba_name->city"),
            FDParser.parseSimpleFD("aka_name,address->city"),
            FDParser.parseSimpleFD("zip,license_number->city"),
            
            FDParser.parseSimpleFD("aka_name,license_number->zip"),           
            FDParser.parseSimpleFD("dba_name,license_number->zip"),
            FDParser.parseSimpleFD("address,dba_name->zip"),
            FDParser.parseSimpleFD("aka_name,address->zip"),
            FDParser.parseSimpleFD("address,license_number->zip")
        );
        
        SwipePartition partition = PartitionGenerator.generate(ruleset);
        
        assertTrue(partition.classIndexOf("license_number") < partition.classIndexOf("zip"));
        assertTrue(partition.classIndexOf("address") < partition.classIndexOf("zip"));
        assertTrue(partition.classIndexOf("dba_name") < partition.classIndexOf("zip"));
        assertTrue(partition.classIndexOf("aka_name") < partition.classIndexOf("zip"));
        assertTrue(partition.classIndexOf("zip") < partition.classIndexOf("city"));
        assertTrue(partition.classIndexOf("city") < partition.classIndexOf("state"));
        
    }
    
    @Test
    public void priorityReliabilityTest1() throws ParseException, RepairException
    {
        RuleSet<Dataset, SimpleFD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("euct->single"),
            FDParser.parseSimpleFD("euct->double"),
            FDParser.parseSimpleFD("euct->open"),
            FDParser.parseSimpleFD("single,double->open"),
            FDParser.parseSimpleFD("double,open->single"),
            FDParser.parseSimpleFD("single,open->double")
        );
        
        //We say that double is more reliable open, we leave single undetermined
        FixedPriority prio = new FixedPriority(ListOperations.list("double", "open"));
        
        List<String> priority = prio.createPriority
        (
            null,
            ruleset,
            SetOperations.set("double", "single", "open")
        );
        
        List<String> expected = ListOperations.list(
            "single",   //First, because undetermined
            "open",     //Second, because less reliable than double
            "double");  //Last
        
        assertEquals(expected, priority);
    }
    
    @Test
    public void cellTest1()
    {
        Cell<String> c1 = new Cell<>("test", 1);
        Cell<String> c2 = new Cell<>("test", 1);
        
        assertTrue(c1.equals(c2));
        assertTrue(c2.equals(c1));
        assertTrue(c1.hashCode() == c2.hashCode());
    }
    
    @Test
    public void cellTest2()
    {
        Cell<String> c1 = new Cell<>("test", 1);
        Cell<String> c2 = new Cell<>("test", 2);
        
        assertTrue(c1.equals(c2));
        assertTrue(c2.equals(c1));
        assertTrue(c1.hashCode() == c2.hashCode());
    }
    
    @Test
    public void cellTest3()
    {
        Cell<String> c1 = new Cell<>("test", 1);
        Cell<String> c2 = new Cell<>(null, 1);
        
        assertFalse(c1.equals(c2));
        assertFalse(c2.equals(c1));
    }
    
    @Test
    public void cellTest4()
    {
        Cell<String> c1 = new Cell<>("test", 1);
        Cell<String> c2 = new Cell<>(null, 2);
        
        assertFalse(c1.equals(c2));
        assertFalse(c2.equals(c1));
    }
    
    @Test
    public void cellTest5()
    {
        Cell<String> c1 = new Cell<>(null, 1);
        Cell<String> c2 = new Cell<>(null, 2);
        
        assertFalse(c1.equals(c2));
        assertFalse(c2.equals(c1));
    }
    
    @Test
    public void cellTest6()
    {
        Cell<String> c1 = new Cell<>(null, 1);
        Cell<String> c2 = new Cell<>(null, 1);
        
        assertTrue(c1.equals(c2));
        assertTrue(c2.equals(c1));
    }
    
    @Test
    public void cellMapTest()
    {
        Cell<String> c1 = new Cell<>(null, 1);
        Cell<String> c2 = new Cell<>(null, 1);
        Cell<String> c3 = new Cell<>("test", 1);
        Cell<String> c4 = new Cell<>("test", 3);
        
        CellMap cm1 = new CellMap(new HashMap<>());
        cm1.update("a", c1);
        cm1.update("b", c3);
        CellMap cm2 = new CellMap(new HashMap<>());
        cm2.update("a", c2);
        cm2.update("b", c4);
        
        assertTrue(cm1.equals(cm2));
    }
    
    @Test
    public void repairTest2() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a -> b"),
            FDParser.parseSimpleFD("c -> a"),
            FDParser.parseSimpleFD("b -> c")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 2).set("c", 1));
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 2).set("c", 2));
        dirty.addDataObject(new DataObject().set("a", 2).set("b", 0).set("c", 2));
        dirty.addDataObject(new DataObject().set("a", 2).set("b", 0).set("c", 3));
        
        Map<String,CostFunction<?>> cfm = new HashMap<>();
        cfm.put("a", new MaxCostFunction());
        cfm.put("b", new MaxCostFunction());
        cfm.put("c", new MaxCostFunction());
        SwipeCostModel cm = new SwipeCostModel(cfm);
        
        Swipe swipe = new Swipe(
            ruleset,
            cm,
            new FixedPriority(ListOperations.list("b", "a", "c"))
        );
        
        Dataset repair = swipe.repair(dirty);
      
        Dataset expected = new SimpleDataset();
        
        expected.addDataObject(new DataObject().set("a", 1).set("b", 2).set("c", 2));
        expected.addDataObject(new DataObject().set("a", 1).set("b", 2).set("c", 2));
        expected.addDataObject(new DataObject().set("a", 2).set("b", 0).set("c", 3));
        expected.addDataObject(new DataObject().set("a", 2).set("b", 0).set("c", 3));
        
        assertEquals(expected, repair);   
    }
    
    @Test
    public void repairTest3() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a -> b"),
            FDParser.parseSimpleFD("b,c -> a")
        );
        
        Dataset dirty = new SimpleDataset();
        
        
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 0));
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 0));
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 2).set("c", 2));
        dirty.addDataObject(new DataObject().set("a", 2).set("b", 2).set("c", 0));
        dirty.addDataObject(new DataObject().set("a", 2).set("b", 2).set("c", 0));
        
        Map<String,CostFunction<?>> cfm = new HashMap<>();
        cfm.put("a", new MaxCostFunction());
        cfm.put("b", new MaxCostFunction());
        SwipeCostModel cm = new SwipeCostModel(cfm);
        
        Swipe swipe = new Swipe(
            ruleset,
            cm,
            new FixedPriority(ListOperations.list("a", "b"))
        );
        
        Dataset repair = swipe.repair(dirty);
      
        Dataset expected = new SimpleDataset();
        
        expected.addDataObject(new DataObject().set("a", 2).set("b", 2).set("c", 0));
        expected.addDataObject(new DataObject().set("a", 2).set("b", 2).set("c", 0));
        expected.addDataObject(new DataObject().set("a", 1).set("b", 2).set("c", 2));
        expected.addDataObject(new DataObject().set("a", 2).set("b", 2).set("c", 0));
        expected.addDataObject(new DataObject().set("a", 2).set("b", 2).set("c", 0));
        
        assertEquals(expected, repair);   
    }
    
    @Test
    public void repairTest4() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("d,c -> a"),
            FDParser.parseSimpleFD("a,b -> c"),
            FDParser.parseSimpleFD("a,b -> e")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 0).set("d", 1).set("e", 0));
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 0).set("d", 0).set("e", 1));
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 0).set("d", 0).set("e", 2));
        dirty.addDataObject(new DataObject().set("a", 2).set("b", 2).set("c", 0).set("d", 1).set("e", 3));
        dirty.addDataObject(new DataObject().set("a", 2).set("b", 2).set("c", 0).set("d", 1).set("e", 4));
        dirty.addDataObject(new DataObject().set("a", 2).set("b", 2).set("c", 0).set("d", 0).set("e", 5));
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 2).set("c", 1).set("d", 3).set("e", 6));

        Map<String,CostFunction<?>> cfm = new HashMap<>();
        cfm.put("a", new ConstantCostFunction());
        cfm.put("c", new MaxCostFunction());
        cfm.put("e", new MaxCostFunction());
        SwipeCostModel cm = new SwipeCostModel(cfm);

        Swipe swipe = new Swipe(
            ruleset,
            cm,
            new FixedPriority(ListOperations.list("a", "c", "e"))
        );
        
        Dataset repair = swipe.repair(dirty);

        Dataset expected = new SimpleDataset();
        
        expected.addDataObject(new DataObject().set("a", 2).set("b", 0).set("c", 0).set("d", 1).set("e", 0));
        expected.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 0).set("d", 0).set("e", 2));
        expected.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 0).set("d", 0).set("e", 2));
        expected.addDataObject(new DataObject().set("a", 2).set("b", 2).set("c", 1).set("d", 1).set("e", 4));
        expected.addDataObject(new DataObject().set("a", 2).set("b", 2).set("c", 1).set("d", 1).set("e", 4));
        expected.addDataObject(new DataObject().set("a", 1).set("b", 2).set("c", 1).set("d", 0).set("e", 6));
        expected.addDataObject(new DataObject().set("a", 1).set("b", 2).set("c", 1).set("d", 3).set("e", 6));
        
        
        assertEquals(expected, repair);   
    }
    
    @Test
    public void repairTest5() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a,b -> c"),
            FDParser.parseSimpleFD("d,c -> a"),
            FDParser.parseSimpleFD("a,c,d -> b"),
            FDParser.parseSimpleFD("b,c -> d")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 0).set("d", 1));
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 2).set("d", 0));
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 2).set("d", 0));
        dirty.addDataObject(new DataObject().set("a", 2).set("b", 2).set("c", 0).set("d", 1));
        dirty.addDataObject(new DataObject().set("a", 2).set("b", 2).set("c", 2).set("d", 1));
        dirty.addDataObject(new DataObject().set("a", 3).set("b", 2).set("c", 0).set("d", 0));
        dirty.addDataObject(new DataObject().set("a", 3).set("b", 2).set("c", 1).set("d", 3));        

        Map<String,CostFunction<?>> cfm = new HashMap<>();
        cfm.put("a", new MaxCostFunction());
        cfm.put("b", new MinCostFunction());
        cfm.put("c", new MinCostFunction());
        cfm.put("d", new MaxCostFunction());
        
        SwipeCostModel cm = new SwipeCostModel(cfm);
        
        Swipe swipe = new Swipe(
            ruleset,
            cm,
            new FixedPriority(ListOperations.list("a", "c", "b", "d"))
        );
        
        Dataset repair = swipe.repair(dirty);
           
        Dataset expected = new SimpleDataset();
        
        expected.addDataObject(new DataObject().set("a", 3).set("b", 0).set("c", 0).set("d", 1));
        expected.addDataObject(new DataObject().set("a", 3).set("b", 0).set("c", 0).set("d", 1));
        expected.addDataObject(new DataObject().set("a", 3).set("b", 0).set("c", 0).set("d", 1));
        expected.addDataObject(new DataObject().set("a", 3).set("b", 0).set("c", 0).set("d", 1));
        expected.addDataObject(new DataObject().set("a", 2).set("b", 2).set("c", 2).set("d", 1));
        expected.addDataObject(new DataObject().set("a", 3).set("b", 0).set("c", 0).set("d", 1));
        expected.addDataObject(new DataObject().set("a", 3).set("b", 2).set("c", 1).set("d", 3)); 
        
        
        assertEquals(expected, repair);   
    }
    
    @Test
    public void repairTest6() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("c -> b"),
            FDParser.parseSimpleFD("e -> a"),
            FDParser.parseSimpleFD("a,b -> e"),
            FDParser.parseSimpleFD("e,c -> d")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(
            new DataObject()
                .set("a", 5)
                .set("c", 1)
                .set("e", 5)
        );
        dirty.addDataObject(
            new DataObject()
                .set("a", 5)
                .set("c", 1)
                .set("d", 2)
        );
        dirty.addDataObject(
            new DataObject()
                .set("c", 2)
                .set("d", 3)
                .set("e", 2)
        );
        dirty.addDataObject(
            new DataObject()
                .set("b", 3)
                .set("c", 3)
                .set("d", 1)
                .set("e", 2)
        );
        dirty.addDataObject(
            new DataObject()
                .set("b", 2)
                .set("c", 4)
                .set("e", 2)
        );
        dirty.addDataObject(
            new DataObject()
                .set("b", 1)
                .set("c", 5)
                .set("e", 5)
        );
        
        Swipe swipe = new Swipe(
            ruleset,
            new SwipeCostModel(ruleset),
            new FixedPriority(ListOperations.list("c,e,b,a,d"))
        );
        
        Dataset repair = swipe.repair(dirty);
        
        Dataset expected = new SimpleDataset();
        
        expected.addDataObject(
            new DataObject()
                .set("a", 5)
                .set("b", null)
                .set("c", 1)
                .set("e", 5)
                .set("d", 2)
        );
        expected.addDataObject(
            new DataObject()
                .set("a", 5)
                .set("b", null)
                .set("c", 1)
                .set("d", 2)
                .set("e", 5)
        );
        expected.addDataObject(
            new DataObject()
                .set("a", null)
                .set("b", null)
                .set("c", 2)
                .set("d", 3)
                .set("e", 2)
        );
        expected.addDataObject(
            new DataObject()
                .set("a", null)
                .set("b", 3)
                .set("c", 3)
                .set("d", 1)
                .set("e", 2)
        );
        expected.addDataObject(
            new DataObject()
                .set("a", null)
                .set("b", 2)
                .set("c", 4)
                .set("d", null)
                .set("e", 2)
        );
        expected.addDataObject(
            new DataObject()
                .set("a", 5)
                .set("b", 1)
                .set("c", 5)
                .set("d", null)
                .set("e", 5)
        );
        
        
        assertEquals(expected, repair);
    }
    
    @Test
    public void repairTestWithNulls() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a,b -> c"),
            FDParser.parseSimpleFD("b,c -> a"),
            FDParser.parseSimpleFD("a,c -> b")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 0));
        dirty.addDataObject(new DataObject().set("b", 0).set("c", 0));
        dirty.addDataObject(new DataObject().set("a", 2).set("b", 1).set("c", 2));
        dirty.addDataObject(new DataObject().set("a", 3).set("b", 2).set("c", 0));
        dirty.addDataObject(new DataObject().set("a", 4).set("b", 3).set("c", 2));
        dirty.addDataObject(new DataObject().set("a", 5).set("b", 3).set("c", 2));

        Map<String,CostFunction<?>> cfm = new HashMap<>();
        cfm.put("a", new MaxCostFunction());
        cfm.put("b", new MinCostFunction());
        cfm.put("c", new MinCostFunction());
        SwipeCostModel cm = new SwipeCostModel(cfm);        

        Swipe swipe = new Swipe(
            ruleset,
            cm,
            new FixedPriority(ListOperations.list("a", "c", "b"))
        );
        
        Dataset repair = swipe.repair(dirty);
        
        Dataset expected = new SimpleDataset();
        
        expected.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 0));
        expected.addDataObject(new DataObject().set("a", 1).set("b", 0).set("c", 0));
        expected.addDataObject(new DataObject().set("a", 2).set("b", 1).set("c", 2));
        expected.addDataObject(new DataObject().set("a", 3).set("b", 2).set("c", 0));
        expected.addDataObject(new DataObject().set("a", 5).set("b", 3).set("c", 2));
        expected.addDataObject(new DataObject().set("a", 5).set("b", 3).set("c", 2));
        
        
        assertEquals(expected, repair);   
    }
    
    @Test
    public void repairTestWithNulls2() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a -> b")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject().set("a", 1).set("b", 0));
        dirty.addDataObject(new DataObject().set("a", null).set("b", 1));
        dirty.addDataObject(new DataObject().set("a", null).set("b", 2));
        

        Swipe swipe = new Swipe(ruleset, new SwipeCostModel(ruleset));
        
        Dataset repair = swipe.repair(dirty);       

        assertEquals(dirty, repair);   
    }
    
    @Test
    public void repairTestWithNulls3() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a -> b"),
            FDParser.parseSimpleFD("b -> c")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject()
            .set("a", 1)
            .set("c", 4)
        );
        dirty.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", null)
            .set("c", 5)
        );      

        Map<String,CostFunction<?>> cfm = new HashMap<>();
        cfm.put("c", new MinCostFunction());

        Swipe swipe = new Swipe(ruleset, new SwipeCostModel(cfm));
        
        Dataset repair = swipe.repair(dirty);       

        assertEquals(2, repair.getSize());
        assertEquals(4, repair.getDataObjects().get(0).getInteger("c").intValue());
        assertEquals(4, repair.getDataObjects().get(1).getInteger("c").intValue());
    }
    
    @Test
    public void repairTestWithNulls4() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a -> b"),
            FDParser.parseSimpleFD("b -> c")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject()
            .set("a", 1).set("c", 4)
        );
        dirty.addDataObject(new DataObject()
            .set("a", 1).set("b", null).set("c", 5)
        );      
        dirty.addDataObject(new DataObject()
            .set("a", 2).set("c", 8)
        );
        dirty.addDataObject(new DataObject()
            .set("a", 2).set("b", null).set("c", 3)
        );      

        Map<String,CostFunction<?>> cfm = new HashMap<>();
        cfm.put("c", new MinCostFunction());
        
        Swipe swipe = new Swipe(ruleset, new SwipeCostModel(cfm));
        
        Dataset repair = swipe.repair(dirty);       

        assertEquals(4, repair.getSize());
        assertEquals(4, repair.getDataObjects().get(0).getInteger("c").intValue());
        assertEquals(4, repair.getDataObjects().get(1).getInteger("c").intValue());
        assertEquals(3, repair.getDataObjects().get(2).getInteger("c").intValue());
        assertEquals(3, repair.getDataObjects().get(3).getInteger("c").intValue());
    }
    
    @Test
    public void repairTestWithNulls5() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a,d -> c"),
            FDParser.parseSimpleFD("b -> d")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject()
            .set("a", 1).set("c", 2).set("d", 1)
        );
        dirty.addDataObject(new DataObject()
            .set("a", 1).set("c", 3).set("d", 1)
        );
        dirty.addDataObject(new DataObject()
            .set("a", 2).set("c", 4).set("d", 1)
        );
        dirty.addDataObject(new DataObject()
            .set("a", 2).set("c", 5).set("d", 1)
        );
        Map<String,CostFunction<?>> cfm = new HashMap<>();
        cfm.put("c", new MinCostFunction());
        
        Swipe swipe = new Swipe(ruleset, new SwipeCostModel(cfm));
        
        Dataset repair = swipe.repair(dirty);       

        assertEquals(4, repair.getSize());
        assertEquals(2, repair.getDataObjects().get(0).getInteger("c").intValue());
        assertEquals(2, repair.getDataObjects().get(1).getInteger("c").intValue());
        assertEquals(4, repair.getDataObjects().get(2).getInteger("c").intValue());
        assertEquals(4, repair.getDataObjects().get(3).getInteger("c").intValue());
    }
    
    @Test
    public void reliabilityEstimateTest1() throws ParseException
    {
        Dataset data = new SimpleDataset();
        
        data.addDataObject(new DataObject().set("a", 0).set("b", 1).set("c", 2));
        data.addDataObject(new DataObject().set("a", 0).set("b", 1).set("c", 2));
        data.addDataObject(new DataObject().set("a", 0).set("b", 1).set("c", 3));
        data.addDataObject(new DataObject().set("a", 1).set("b", 3));
        data.addDataObject(new DataObject().set("a", 2).set("b", 4).set("c", 2));
        
        ReliabilityPriority p = new ReliabilityPriority();
        
        SwipeRepair sr = new SwipeRepair(data, SetOperations.set("a","b","c"));
        
        Set<Long> viol = p.violationTuples(sr, FDParser.parseSimpleFD("a,b -> c"));
        
        assertEquals(SetOperations.set(2l), viol);
    }
    
    @Test
    public void reliabilityEstimateTest2() throws ParseException
    {
        Dataset data = new SimpleDataset();
        
        data.addDataObject(new DataObject().set("a", 0).set("b", 1).set("c", 2));
        data.addDataObject(new DataObject().set("a", 0).set("b", 1).set("c", 2));
        data.addDataObject(new DataObject().set("a", 0).set("b", 1).set("c", 3));
        data.addDataObject(new DataObject().set("a", 1).set("b", 3));
        data.addDataObject(new DataObject().set("a", 2).set("b", 4).set("c", 2));
        
        ReliabilityPriority p = new ReliabilityPriority();
        
        SwipeRepair sr = new SwipeRepair(data,SetOperations.set("a","c"));
        
        Set<Long> viol = p.violationTuples(sr, FDParser.parseSimpleFD("c -> a"));
        
        assertEquals(SetOperations.set(4l), viol);
    }
    
    @Test
    public void reliabilityEstimateTest3() throws ParseException
    {
        Dataset data = new SimpleDataset();
        
        data.addDataObject(new DataObject().set("a", 0).set("b", 1));
        data.addDataObject(new DataObject().set("a", 0).set("b", 2));
        data.addDataObject(new DataObject().set("a", 1).set("b", 3));
        data.addDataObject(new DataObject().set("a", 1).set("b", 4));
        data.addDataObject(new DataObject().set("a", 2).set("b", null));
        data.addDataObject(new DataObject().set("a", 2).set("b", null));
        data.addDataObject(new DataObject().set("a", 2).set("b", null));
        
        ReliabilityPriority p = new ReliabilityPriority();
        
        SwipeRepair sr = new SwipeRepair(data, SetOperations.set("a","b"));
        
        Set<Long> viol = p.violationTuples(sr, FDParser.parseSimpleFD("a -> b"));
        
        assertTrue(
                (viol.contains(0l) && !viol.contains(1l))
            ||  (!viol.contains(0l) && viol.contains(1l))
        );
        
        assertTrue(
                (viol.contains(2l) && !viol.contains(3l))
            ||  (!viol.contains(2l) && viol.contains(3l))
        );
        
        assertTrue(
                (viol.contains(4l) && viol.contains(5l) && !viol.contains(6l))
            ||  (viol.contains(4l) && !viol.contains(5l) && viol.contains(6l))
            ||  (!viol.contains(4l) && viol.contains(5l) && viol.contains(6l))
        );
    }
    
    @Test
    public void dsfTest() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("hospital_name -> provider")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject()
            .set("hospital_name", "ECM hospital")
            .set("provider", "10006"));
        dirty.addDataObject(new DataObject()
            .set("hospital_name", "ECM hospital")
            .set("provider", "10006"));
        dirty.addDataObject(new DataObject()
            .set("hospital_name", "ECM hospital")
            .set("provider", "10006"));
        dirty.addDataObject(new DataObject()
            .set("hospital_name", "ECM hospital")
            .set("provider", "1000x"));
        dirty.addDataObject(new DataObject()
            .set("hospital_name", "huntsville hospital")
            .set("provider", "1003x"));
        dirty.addDataObject(new DataObject()
            .set("hospital_name", "huntsville hospital")
            .set("provider", "10039"));

        Swipe swipe = new Swipe(ruleset,new SwipeCostModel(ruleset));
        
        Swipe.VERBOSITY = 6;
        
        Dataset repair = swipe.repair(dirty);
        
        repair
            .getDataObjects()
            .stream()
            .forEach(System.out::println);
    }
    
    @Test
    public void weightVoteTest() throws ParseException, RepairException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseSimpleFD("a -> b")
        );
        
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject().set("a", 0).set("b", 0).set("w", 1));
        dirty.addDataObject(new DataObject().set("a", 0).set("b", 1).set("w", 2));
        
        Map<String,CostFunction<?>> cfm = new HashMap<>();
        cfm.put("b", new WeightedCostFunction("w"));
        
        Swipe swipe = new Swipe(ruleset, new SwipeCostModel(cfm));
        
        Dataset repair = swipe.repair(dirty);
        
        System.out.println(repair);

        assertTrue(repair.project("a","b").getDataObjects().get(0).equals(new DataObject().set("a", 0).set("b", 1)));   
    }
}
