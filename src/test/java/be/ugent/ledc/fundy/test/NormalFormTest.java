package be.ugent.ledc.fundy.test;

import be.ugent.ledc.core.binding.jdbc.agents.PersistentDatatype;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.normalization.NormalForm;
import be.ugent.ledc.fundy.algorithms.normalization.NormalFormVerifier;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.NormalisationException;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import be.ugent.ledc.core.util.SetOperations;
import java.util.Set;
import org.junit.Test;
import static org.junit.Assert.*;

public class NormalFormTest
{
    @Test
    public void normalForm2NFTest1() throws NormalisationException
    {
        
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("B"), "A"),
            new SimpleFD(SetOperations.set("B"), "D"),
            new SimpleFD(SetOperations.set("B", "C"), "E"),
            new SimpleFD(SetOperations.set("D", "G"), "F"),
            new SimpleFD(SetOperations.set("E", "F"), "G")
        );
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D", "E", "F", "G");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }

        //Schema is not in 2NF
        assertFalse(new NormalFormVerifier().hasNormalForm(tableSchema, ruleset, NormalForm._2NF));
    }
    
    @Test
    public void normalForm2NFTest2() throws NormalisationException
    {
        
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("B"), "C"),
            new SimpleFD(SetOperations.set("D"), "E"),
            new SimpleFD(SetOperations.set("E"), "D")
        );
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D", "E", "F");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        assertFalse(new NormalFormVerifier().hasNormalForm(tableSchema, ruleset, NormalForm._2NF));
    }
    
    @Test
    public void normalForm2NFTest3() throws NormalisationException
    {
        
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("A", "B"), "C"),
            new SimpleFD(SetOperations.set("B", "C"), "A"),
            new SimpleFD(SetOperations.set("A", "C"), "D"),
            new SimpleFD(SetOperations.set("D", "E"), "F"),
            new SimpleFD(SetOperations.set("E", "F"), "G"),
            new SimpleFD(SetOperations.set("A", "G"), "E")
        );
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D", "E", "F", "G");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        assertFalse(new NormalFormVerifier().hasNormalForm(tableSchema, ruleset, NormalForm._2NF));
    }
    
    @Test
    public void normalForm2NFTest4() throws NormalisationException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "D"),
            new SimpleFD(SetOperations.set("D"), "A"),
            new SimpleFD(SetOperations.set("B", "C"), "E"),
            new SimpleFD(SetOperations.set("B", "C"), "F"),
            new SimpleFD(SetOperations.set("D", "E"), "G"),
            new SimpleFD(SetOperations.set("F"), "G"),
            new SimpleFD(SetOperations.set("G"), "F"),
            new SimpleFD(SetOperations.set("G"), "H")
        );
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D", "E", "F", "G", "H");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        assertFalse(new NormalFormVerifier().hasNormalForm(tableSchema, ruleset, NormalForm._2NF));
    }
    
    @Test
    public void normalForm2NFTest5() throws NormalisationException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "C"),
            new SimpleFD(SetOperations.set("A"), "E"),
            new SimpleFD(SetOperations.set("C"), "A"),
            new SimpleFD(SetOperations.set("B"), "F"),
            new SimpleFD(SetOperations.set("F"), "B"),
            new SimpleFD(SetOperations.set("B", "C"), "D"),
            new SimpleFD(SetOperations.set("A", "B"), "D")
        );
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D", "E", "F");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        assertFalse(new NormalFormVerifier().hasNormalForm(tableSchema, ruleset, NormalForm._2NF));
    }
    
    @Test
    public void normalForm2NFTest6() throws NormalisationException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("B"), "C"),
            new SimpleFD(SetOperations.set("C"), "B"),
            new SimpleFD(SetOperations.set("B", "D"), "E"),
            new SimpleFD(SetOperations.set("C"), "F"),
            new SimpleFD(SetOperations.set("F"), "C"),
            new SimpleFD(SetOperations.set("E", "F"), "G")
        );
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D", "E", "F", "G");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        assertFalse(new NormalFormVerifier().hasNormalForm(tableSchema, ruleset, NormalForm._2NF));
    }
    
    @Test
    public void normalForm2NFTest7() throws NormalisationException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>();
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }        
        assertTrue(new NormalFormVerifier().hasNormalForm(tableSchema, ruleset, NormalForm._2NF));
    }
    
    @Test
    public void normalForm2NFTest8() throws NormalisationException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("B"), "C"),
            new SimpleFD(SetOperations.set("C", "E"), "A"),
            new SimpleFD(SetOperations.set("D"), "E")
        );
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D", "E");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        assertFalse(new NormalFormVerifier().hasNormalForm(tableSchema, ruleset, NormalForm._2NF));
    }
    
    @Test
    public void normalForm3NFTest1() throws NormalisationException
    {
        
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("teacher"), "department"),
            new SimpleFD(SetOperations.set("department"), "location")
        );
        
        Set<String> attributes = SetOperations.set("teacher", "department", "location");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("teachers");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }

        //Schema is in 2NF
        assertTrue(new NormalFormVerifier().hasNormalForm(tableSchema, ruleset, NormalForm._2NF));
        //Schema is not in 3NF
        assertFalse(new NormalFormVerifier().hasNormalForm(tableSchema, ruleset, NormalForm._3NF));
    }
    
    @Test
    public void normalFormBCNFTest1() throws NormalisationException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("name"), "id"),
            new SimpleFD(SetOperations.set("id"), "name"),
            new SimpleFD(SetOperations.set("id","teacher"), "type"),
            new SimpleFD(SetOperations.set("name","teacher"), "type")
        );
        
        Set<String> attributes = SetOperations.set("teacher", "name", "id", "type");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("courses");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }

        //Schema is in 2NF & 3NF
        assertTrue(new NormalFormVerifier().hasNormalForm(tableSchema, ruleset, NormalForm._2NF));
        assertTrue(new NormalFormVerifier().hasNormalForm(tableSchema, ruleset, NormalForm._3NF));
        
        //Schema is in not in BCNF
        assertFalse(new NormalFormVerifier().hasNormalForm(tableSchema, ruleset, NormalForm.BCNF));
    }
}
