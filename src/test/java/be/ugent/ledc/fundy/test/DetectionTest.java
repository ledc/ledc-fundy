package be.ugent.ledc.fundy.test;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.detection.Detector;
import be.ugent.ledc.fundy.algorithms.detection.FDViolation;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.io.FDParser;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.junit.Assert;
import org.junit.Test;

public class DetectionTest
{
    @Test
    public void detectionTest1() throws ParseException
    {
        FD fd = FDParser.parseFD("a->b");
        RuleSet<Dataset, FD> ruleset = new RuleSet<>(fd);
        
        SimpleDataset data = new SimpleDataset();
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", 1)
        );
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", 1)
        );
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", 2)
        );
        
        data.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", 2)
        );
        
        data.addDataObject(new DataObject()
            .set("a", 2)
            .set("b", 2)
        );
        
        Map<FD, Set<FDViolation>> errors = Detector.detectErrors(data, ruleset);
        
        Map<DataObject,Long> countMap = new HashMap<>();

        countMap.put(new DataObject().set("b", 1), 2l);
        countMap.put(new DataObject().set("b", 2), 1l);
        
        Map<FD, Set<FDViolation>> expected = new HashMap<>();
        expected.put(fd, new HashSet<>());
        expected.get(fd).add(new FDViolation(new DataObject().set("a", 1), countMap));
        
        Assert.assertTrue(errors.size() == 1);
        Assert.assertTrue(errors.get(fd).size() == 1);
        Assert.assertEquals(expected, errors);

    }
    
    @Test
    public void satisfactionTest1() throws ParseException
    {
        FD fd = FDParser.parseFD("a->b");
        
        SimpleDataset data = new SimpleDataset();
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", 1)
        );
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", 1)
        );

        
        Assert.assertTrue(fd.isSatisfied(data));
        Assert.assertFalse(fd.isFailed(data));
    }
    
    @Test
    public void satisfactionTest2() throws ParseException
    {
        FD fd = FDParser.parseFD("a->b");
        
        SimpleDataset data = new SimpleDataset();
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", 1)
        );
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", 2)
        );

        
        Assert.assertFalse(fd.isSatisfied(data));
        Assert.assertTrue(fd.isFailed(data));
    }
    
    @Test
    public void satisfactionTest3() throws ParseException
    {
        FD fd = FDParser.parseFD("a->b");
        
        SimpleDataset data = new SimpleDataset();
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", 1)
        );
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", null)
        );

        
        Assert.assertFalse(fd.isSatisfied(data));
        Assert.assertTrue(fd.isFailed(data));
    }
    
    @Test
    public void satisfactionTest4() throws ParseException
    {
        FD fd = FDParser.parseFD("a->b");
        
        SimpleDataset data = new SimpleDataset();
        
        data.addDataObject(new DataObject()
            .set("a", null)
            .set("b", 1)
        );
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("b", null)
        );

        
        Assert.assertTrue(fd.isSatisfied(data));
        Assert.assertFalse(fd.isFailed(data));
    }
    
    @Test
    public void satisfactionTest5() throws ParseException
    {
        FD fd = FDParser.parseFD("a,c->b");
        
        SimpleDataset data = new SimpleDataset();
        
        data.addDataObject(new DataObject()
            .set("a", null)
            .set("c", 1)
            .set("b", 1)
        );
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("c", 1)
            .set("b", null)
        );

        
        Assert.assertTrue(fd.isSatisfied(data));
        Assert.assertFalse(fd.isFailed(data));
    }
    
    @Test
    public void satisfactionTest6() throws ParseException
    {
        FD fd = FDParser.parseFD("a,c->b");
        
        SimpleDataset data = new SimpleDataset();
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("c", 1)
            .set("b", 1)
        );
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("c", 1)
            .set("b", 1)
        );
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("c", 1)
            .set("b", null)
        );
        
        data.addDataObject(new DataObject()
            .set("a", 1)
            .set("c", 1)
            .set("b", null)
        );

        
        Assert.assertFalse(fd.isSatisfied(data));
        Assert.assertTrue(fd.isFailed(data));
    }
}
