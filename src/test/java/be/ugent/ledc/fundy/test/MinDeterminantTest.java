package be.ugent.ledc.fundy.test;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.algorithms.reasoning.MinimalDeterminantFinder;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.io.FDParser;
import java.util.HashSet;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class MinDeterminantTest
{
    @Test
    public void minDeterminantTest1() throws ParseException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
                FDParser.parseFD("a->g"),
                FDParser.parseFD("b->g"),
                FDParser.parseFD("c->h"),
                FDParser.parseFD("e->h"),
                FDParser.parseFD("g,h->f")
        );
        
        Set<Set<String>> expected = new HashSet<>();
        
        expected.add(SetOperations.set("a", "c"));
        expected.add(SetOperations.set("a", "e"));
        expected.add(SetOperations.set("a", "h"));
        expected.add(SetOperations.set("b", "c"));
        expected.add(SetOperations.set("b", "e"));
        expected.add(SetOperations.set("b", "h"));
        expected.add(SetOperations.set("c", "g"));
        expected.add(SetOperations.set("e", "g"));
        expected.add(SetOperations.set("g", "h"));
        
        assertEquals(expected, MinimalDeterminantFinder.findMinimalDeterminants(ruleset, "f"));
    }
    
    @Test
    public void minDeterminantTest2() throws ParseException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
                FDParser.parseFD("a->b"),
                FDParser.parseFD("b->c"),
                FDParser.parseFD("c->e"),
                FDParser.parseFD("e->f"),
                FDParser.parseFD("a,c->f")
        );
        
        Set<Set<String>> expected = new HashSet<>();
        
        expected.add(SetOperations.set("a"));
        expected.add(SetOperations.set("b"));
        expected.add(SetOperations.set("c"));
        expected.add(SetOperations.set("e"));

        assertEquals(expected, MinimalDeterminantFinder.findMinimalDeterminants(ruleset, "f"));
    }
    
    @Test
    public void minDeterminantTest3() throws ParseException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            FDParser.parseFD("euct->s"),
            FDParser.parseFD("euct->d"),
            FDParser.parseFD("euct->o"),
            FDParser.parseFD("s,d->o"),
            FDParser.parseFD("s,o->d"),
            FDParser.parseFD("o,d->s")
        );
        
        Set<Set<String>> expected = new HashSet<>();
        
        expected.add(SetOperations.set("euct"));
        expected.add(SetOperations.set("d", "o"));

        assertEquals(expected, MinimalDeterminantFinder.findMinimalDeterminants(ruleset, "s"));
    }
}
