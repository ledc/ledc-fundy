package be.ugent.ledc.fundy.test;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.binding.jdbc.agents.PersistentDatatype;
import be.ugent.ledc.core.binding.jdbc.schema.Key;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.core.util.ListOperations;
import be.ugent.ledc.fundy.algorithms.reasoning.CandidateKeyGenerator;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.NormalisationException;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.io.FDParser;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class CandidateKeyTest
{
    @Test
    public void candidateKeyTest1() throws NormalisationException
    {
        
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("B"), "A"),
            new SimpleFD(SetOperations.set("B"), "D"),
            new SimpleFD(SetOperations.set("B", "C"), "E"),
            new SimpleFD(SetOperations.set("D", "G"), "F"),
            new SimpleFD(SetOperations.set("E", "F"), "G")
        );
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D", "E", "F", "G");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        CandidateKeyGenerator ckg = new CandidateKeyGenerator(tableSchema);
        
        Set<Key> expected = new HashSet<>();
        expected.add(new Key(SetOperations.set("C", "A", "G")));
        expected.add(new Key(SetOperations.set("C", "B", "G")));
        expected.add(new Key(SetOperations.set("C", "A", "F")));
        expected.add(new Key(SetOperations.set("C", "B", "F")));
        
        Set<Key> result = ckg.generate(ruleset);
        
        assertEquals(expected, result);
    }
    
    @Test
    public void candidateKeyTest2() throws NormalisationException
    {
        
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("B"), "C"),
            new SimpleFD(SetOperations.set("D"), "E"),
            new SimpleFD(SetOperations.set("E"), "D")
        );
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D", "E", "F");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        CandidateKeyGenerator ckg = new CandidateKeyGenerator(tableSchema);
        
        Set<Key> result = ckg.generate(ruleset);
        
        Set<Key> expected = new HashSet<>();
        expected.add(new Key(SetOperations.set("A", "F", "D")));
        expected.add(new Key(SetOperations.set("A", "F", "E")));
        
        assertEquals(expected, result);
    }
    
    @Test
    public void candidateKeyTest3() throws NormalisationException
    {
        
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("A", "B"), "C"),
            new SimpleFD(SetOperations.set("B", "C"), "A"),
            new SimpleFD(SetOperations.set("A", "C"), "D"),
            new SimpleFD(SetOperations.set("D", "E"), "F"),
            new SimpleFD(SetOperations.set("E", "F"), "G"),
            new SimpleFD(SetOperations.set("A", "G"), "E")
        );
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D", "E", "F", "G");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        CandidateKeyGenerator ckg = new CandidateKeyGenerator(tableSchema);
        
        Set<Key> expected = new HashSet<>();
        expected.add(new Key(SetOperations.set("A", "E")));
        expected.add(new Key(SetOperations.set("A", "G")));
        expected.add(new Key(SetOperations.set("C", "B", "E")));
        expected.add(new Key(SetOperations.set("C", "B", "G")));
        
        Set<Key> result = ckg.generate(ruleset);
        
        assertEquals(expected, result);
    }
    
    @Test
    public void candidateKeyTest4() throws NormalisationException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "D"),
            new SimpleFD(SetOperations.set("D"), "A"),
            new SimpleFD(SetOperations.set("B", "C"), "E"),
            new SimpleFD(SetOperations.set("B", "C"), "F"),
            new SimpleFD(SetOperations.set("D", "E"), "G"),
            new SimpleFD(SetOperations.set("F"), "G"),
            new SimpleFD(SetOperations.set("G"), "F"),
            new SimpleFD(SetOperations.set("G"), "H")
        );
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D", "E", "F", "G", "H");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        CandidateKeyGenerator ckg = new CandidateKeyGenerator(tableSchema);
        
        Set<Key> expected = new HashSet<>();
        expected.add(new Key(SetOperations.set("A", "B", "C")));
        expected.add(new Key(SetOperations.set("D", "B", "C")));
        
        Set<Key> result = ckg.generate(ruleset);
        
        assertEquals(expected, result);
    }
    
    @Test
    public void candidateKeyTest5() throws NormalisationException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "C"),
            new SimpleFD(SetOperations.set("A"), "E"),
            new SimpleFD(SetOperations.set("C"), "A"),
            new SimpleFD(SetOperations.set("B"), "F"),
            new SimpleFD(SetOperations.set("F"), "B"),
            new SimpleFD(SetOperations.set("B", "C"), "D"),
            new SimpleFD(SetOperations.set("A", "B"), "D")
        );
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D", "E", "F");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        CandidateKeyGenerator ckg = new CandidateKeyGenerator(tableSchema);
        
        Set<Key> expected = new HashSet<>();
        expected.add(new Key(SetOperations.set("A", "B")));
        expected.add(new Key(SetOperations.set("A", "F")));
        expected.add(new Key(SetOperations.set("B", "C")));
        expected.add(new Key(SetOperations.set("F", "C")));
        
        Set<Key> result = ckg.generate(ruleset);
        
        assertEquals(expected, result);
    }
    
    @Test
    public void candidateKeyTest6() throws NormalisationException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("B"), "C"),
            new SimpleFD(SetOperations.set("C"), "B"),
            new SimpleFD(SetOperations.set("B", "D"), "E"),
            new SimpleFD(SetOperations.set("C"), "F"),
            new SimpleFD(SetOperations.set("F"), "C"),
            new SimpleFD(SetOperations.set("E", "F"), "G")
        );
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D", "E", "F", "G");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        CandidateKeyGenerator ckg = new CandidateKeyGenerator(tableSchema);
        
        Set<Key> expected = new HashSet<>();
        expected.add(new Key(SetOperations.set("A", "D")));
        
        Set<Key> result = ckg.generate(ruleset);
        
        assertEquals(expected, result);
    }
    
    @Test
    public void candidateKeyTest7() throws NormalisationException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>();
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        CandidateKeyGenerator ckg = new CandidateKeyGenerator(tableSchema);
        
        Set<Key> expected = new HashSet<>();
        expected.add(new Key(SetOperations.set("A", "B", "C", "D")));
        
        Set<Key> result = ckg.generate(ruleset);
        
        assertEquals(expected, result);
    }
    
    @Test
    public void candidateKeyTest8() throws NormalisationException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new SimpleFD(SetOperations.set("A"), "B"),
            new SimpleFD(SetOperations.set("B"), "C"),
            new SimpleFD(SetOperations.set("C", "E"), "A"),
            new SimpleFD(SetOperations.set("D"), "E")
        );
        
        Set<String> attributes = SetOperations.set("A", "B", "C", "D", "E");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        CandidateKeyGenerator ckg = new CandidateKeyGenerator(tableSchema);
        
        Set<Key> expected = new HashSet<>();
        expected.add(new Key(SetOperations.set("A", "D")));
        expected.add(new Key(SetOperations.set("B", "D")));
        expected.add(new Key(SetOperations.set("C", "D")));
        
        Set<Key> result = ckg.generate(ruleset);
        
        assertEquals(expected, result);
    }
    
    @Test
    public void candidateKeyTest9() throws NormalisationException
    {
        RuleSet<Dataset, FD> ruleset = new RuleSet<>
        (
            new FD(SetOperations.set("sid"), SetOperations.set("titel","naam","geboren","periode","eigenaar")),
            new SimpleFD(SetOperations.set("titel","naam"), "sid"),
            new SimpleFD(SetOperations.set("naam"), "geboren")
        );
        
        Set<String> attributes = SetOperations.set("sid", "titel","naam","geboren","periode","eigenaar");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        CandidateKeyGenerator ckg = new CandidateKeyGenerator(tableSchema);
        
        Set<Key> expected = new HashSet<>();
        expected.add(new Key(SetOperations.set("sid")));
        expected.add(new Key(SetOperations.set("titel", "naam")));
        
        Set<Key> result = ckg.generate(ruleset);
        
        assertEquals(expected, result);
    }
    
    @Test
    public void candidateKeyTest10() throws NormalisationException, ParseException
    {
        RuleSet<Dataset, FD> ruleset = FDParser.parseRuleset(ListOperations.list(
            "a->b",
            "b->c",
            "->d"
        ));
        
        Set<String> attributes = SetOperations.set("a","b","c","d");
        
        TableSchema tableSchema = new TableSchema();
        tableSchema.setName("test_table");
        
        for(String attribute: attributes)
        {
            tableSchema.setAttributeProperty(attribute, TableSchema.PROP_DATATYPE, PersistentDatatype.TEXT);
        }
        
        CandidateKeyGenerator ckg = new CandidateKeyGenerator(tableSchema);
        
        Set<Key> expected = new HashSet<>();
        expected.add(new Key(SetOperations.set("a")));
        
        Set<Key> result = ckg.generate(ruleset);
        
        assertEquals(expected, result);
    }
}
