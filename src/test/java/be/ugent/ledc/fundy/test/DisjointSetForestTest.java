package be.ugent.ledc.fundy.test;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.fundy.datastructures.DisjointSetForest;
import be.ugent.ledc.fundy.datastructures.DecoratedForest;
import be.ugent.ledc.fundy.io.FDParser;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class DisjointSetForestTest
{
    @Test
    public void equivalenceTest() throws ParseException
    {
        DisjointSetForest dsf = new DisjointSetForest();
        
        dsf.add(1L);
        dsf.add(2L);
        dsf.add(3L);
        dsf.add(4L);
        dsf.add(5L);
        dsf.add(6L);
        
        dsf.merge(1L, 3L);
        dsf.merge(6L, 3L);
        
        assertTrue(dsf.equivalent(1L, 6L));
        assertFalse(dsf.equivalent(1L, 2L));
        assertFalse(dsf.equivalent(1L, 4L));
        assertFalse(dsf.equivalent(1L, 5L));

    }
    
    
    @Test
    public void equivalenceTest2() throws ParseException
    {
        DecoratedForest dsf = new DecoratedForest(FDParser.parseSimpleFD("a->b"));
        
        dsf.add(1L, new DataObject().set("a", 1));
        dsf.add(2L, new DataObject().set("a", 1));
        dsf.add(3L, new DataObject().set("a", 2));
        dsf.add(4L, new DataObject().set("a", 2));
        
        
        assertTrue(dsf.equivalent(1L, 2L));
        assertTrue(dsf.equivalent(3L, 4L));
        assertFalse(dsf.equivalent(1L, 3L));
        assertFalse(dsf.equivalent(2L, 4L));

    }
    
    @Test
    public void moveTest() throws ParseException
    {
        DecoratedForest dsf = new DecoratedForest(FDParser.parseSimpleFD("a->b"));
        
        dsf.add(1L, new DataObject().set("a", 1));
        dsf.add(2L, new DataObject().set("a", 1));
        dsf.add(3L, new DataObject().set("a", 2));
        dsf.add(4L, new DataObject().set("a", 2));
        dsf.add(5L, new DataObject().set("a", 3));
        
        
        assertTrue(dsf.equivalent(1L, 2L));
        assertTrue(dsf.equivalent(3L, 4L));
        assertFalse(dsf.equivalent(1L, 3L));
        assertFalse(dsf.equivalent(2L, 4L));
        assertFalse(dsf.equivalent(3L, 5L));
        
        dsf.move(3L, new DataObject().set("a", 3));
        
        assertFalse(dsf.equivalent(4L, 3L));
        assertTrue(dsf.equivalent(5L, 3L));

    }
}
