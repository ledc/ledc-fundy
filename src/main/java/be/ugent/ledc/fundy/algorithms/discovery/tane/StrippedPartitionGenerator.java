package be.ugent.ledc.fundy.algorithms.discovery.tane;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.fundy.datastructures.StrippedPartition;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StrippedPartitionGenerator
{
    public static Map<Set<String>, StrippedPartition> generateSingletonPartitions(Set<String> schema, Dataset dataset)
    {
        Map<Set<String>, StrippedPartition> singletonPartitions = new HashMap<>();
        
        for(String attribute: schema)
        {
            Map<Object, Set<Integer>> singletonPartition = new HashMap<>();
            
            for(int i=0; i<dataset.getDataObjects().size(); i++)
            {
                DataObject o = dataset.getDataObjects().get(i);
                Object value = o.get(attribute);
                
                if(value != null)
                {
                    if(!singletonPartition.containsKey(value))
                    {
                        singletonPartition.put(value, new HashSet<>());
                    }
                    singletonPartition.get(value).add(i);
                }
            }
            
            StrippedPartition partition = new StrippedPartition(singletonPartition
                .values()
                .stream()
                .collect(Collectors.toList()), dataset.getSize());
            
            singletonPartitions.put(Stream.of(attribute).collect(Collectors.toSet()), partition);
        }
        
        return singletonPartitions;
    }
}
