package be.ugent.ledc.fundy.algorithms.reasoning;

import be.ugent.ledc.core.binding.jdbc.schema.Key;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.NormalisationException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A class that generates all candidate keys for a given schema and a given set
 * of functional dependencies.
 * @author abronsel
 */
public class CandidateKeyGenerator
{
    private final TableSchema tableSchema;

    public CandidateKeyGenerator(TableSchema tableSchema)
    {
        this.tableSchema = tableSchema;
    }
    
    public Set<Key> generate(RuleSet<Dataset, FD> ruleset) throws NormalisationException
    {
        //Sanity check
        for(FD fd: ruleset)
            if(!tableSchema.getAttributeNames().containsAll(fd.getInvolvedAttributes()))
                throw new NormalisationException("Cannot determine candidate keys for table " + tableSchema.getName()
                    + ": FD " + fd + " has unknown attributes.");
        
        //We start with searching for core attributes that MUST be part of any key
        Set<String> core = new HashSet<>();
        
        //An attribute is a core attribute if
        // a. It does not occur in any FD
        // b. It only occurs in LHS
        core.addAll(tableSchema
            .getAttributeNames()
            .stream()
            .filter(a -> ruleset
                .stream()
                .noneMatch(fd -> fd.getRightHandSide().contains(a)))
            .collect(Collectors.toSet())
        );
        
        AttributeClosureGenerator acg = new AttributeClosureGenerator(ruleset);
        
        if(acg.generate(core).equals(tableSchema.getAttributeNames()))
        {
            return Stream
                .of(core)
                .map(set -> new Key(set))
                .collect(Collectors.toSet());
        }
        
        //Compose combinations to test
        Map<Set<String>, Set<String>> currentLevel = tableSchema
            .getAttributeNames()
            .stream()
            .filter(a -> !core.contains(a)) // Remove core attributes
            .filter(a -> ruleset.stream().anyMatch(fd -> fd.getLeftHandSide().contains(a)))
            .map(a -> Stream.concat(Stream.of(a), core.stream())
                .collect(Collectors.toSet()))
            .collect(Collectors.toMap(
                set->set, 
                set -> acg.generate(set))
            );
        
        boolean stop = false;
        
        //Clean
//        currentLevel
//            .keySet()
//            .removeIf(key -> currentLevel
//                .entrySet()
//                .stream()
//                .anyMatch(e -> !e.getKey().equals(key) // A different entry
//                    && e.getValue().containsAll(key)   // Key is in the closure
//                    && !currentLevel.get(key).containsAll(e.getKey()) // The closure of key does not contain other key
//                ));
        
        while(!stop)
        {
            //Generate next level
            Map<Set<String>, Set<String>> nextLevel = nextLevel(currentLevel, ruleset, acg);
            
            currentLevel.clear();
            currentLevel.putAll(nextLevel);
            
            //Clean
            currentLevel
            .keySet()
            .removeIf(key -> currentLevel
                .entrySet()
                .stream()
                .anyMatch(e -> !e.getKey().equals(key) // A different entry
                    && key.containsAll(e.getKey())   // Key is a superset
                ));
            
            stop = currentLevel
                .values()
                .stream()
                .allMatch(v -> v.equals(tableSchema.getAttributeNames()));
//            System.out.println("Next level after cleaning");
//            currentLevel.entrySet().stream().forEach(System.out::println);
        }
        
        return currentLevel.keySet().stream().map(v->new Key(v)).collect(Collectors.toSet());
    }

    private Map<Set<String>, Set<String>> nextLevel(Map<Set<String>, Set<String>> currentLevel, RuleSet<Dataset, FD> ruleset, AttributeClosureGenerator acg)
    {
        Map<Set<String>, Set<String>> nextLevel = new HashMap<>();
        
        for(Set<String> candidate: currentLevel.keySet())
        {
            //If candidate is a key, pass it to the next level
            if(currentLevel.get(candidate).equals(tableSchema.getAttributeNames()))
            {
                nextLevel.put(candidate, currentLevel.get(candidate));
                continue;
            }
            
            Set<String> attributesToCombine = tableSchema
                .getAttributeNames()
                .stream()
                .filter(a -> !currentLevel
                    .get(candidate)
                    .contains(a))
                .filter(a -> ruleset
                    .stream()
                    .anyMatch(fd -> fd.getLeftHandSide().contains(a)
                        && !currentLevel.get(candidate).containsAll(fd.getRightHandSide()))
                )
                .collect(Collectors.toSet());
            
            for(String a: attributesToCombine)
            {
                Set<String> newCandidate = new HashSet<>();
                newCandidate.addAll(candidate);
                newCandidate.add(a);
                
                nextLevel.put(newCandidate, acg.generate(newCandidate));
            }
        }
        
        return nextLevel;
    }
}
