package be.ugent.ledc.fundy.algorithms.repair.priority;

import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.repair.Cell;
import be.ugent.ledc.fundy.algorithms.repair.CellMap;
import be.ugent.ledc.fundy.algorithms.repair.SwipeRepair;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * A priority model where priority is configured based on increasing reliability.
 * Reliability of an attribute is determined based on an estimate
 * of the number of violations of FDs that have this attribute in the RHS.
 * @author abronsel
 */
public class ReliabilityPriority implements PriorityManager
{
    /**
     * Indicates if the estimate of reliability needs to be done based on the
     * original data or the partial repair constructed so far.
     */
    private final boolean dirtyEstimate;

    public ReliabilityPriority(boolean dirtyEstimate)
    {
        this.dirtyEstimate = dirtyEstimate;
    }

    public ReliabilityPriority()
    {
        this(false);
    }
    
    @Override
    public List<String> createPriority(SwipeRepair sr, RuleSet<Dataset, SimpleFD> fds, Set<String> repairClass)
    {
        if(repairClass.size() == 1)
            return new ArrayList<>(repairClass);
        
        //Keep track of violations estimate
        Map<String, Integer> violations = new HashMap<>();
        
        for(String a: repairClass)
        {
            violations.put(
                a,
                estimateViolations(
                    sr,
                    fds
                        .stream()
                        .filter(fd -> fd.getRhsAttribute().equals(a))
                        .collect(Collectors.toSet())
                )
            );
        }
        
        return violations
            .keySet()
            .stream()
            .sorted(Comparator.<String,Integer>comparing(key -> violations.get(key)).reversed())
            .collect(Collectors.toList());
    }
    
    private int estimateViolations(SwipeRepair sr, Set<SimpleFD> fds)
    {
        Set<Long> violatingTuples = new HashSet<>();
        
        for(SimpleFD fd: fds)
        {
            violatingTuples.addAll(violationTuples(sr, fd));
        }
        
        return violatingTuples.size();
                
    }
    
    public Set<Long> violationTuples(SwipeRepair sr, SimpleFD fd)
    {
        Set<Long> viol = new HashSet<>();
        
        //Initialize a mapping
        Map<CellMap, Map<Cell<?>, Set<Long>>> lhsValues = new HashMap<>();

        //In order to repair an FD X -> a, we partition data on equal values for X
        for(Long id: sr.getIdMapping().keySet())
        {
            //Get object, based on dirty estimate flag choose original or current
            CellMap cellMap = dirtyEstimate
                ? sr.getIdMapping()
                    .get(id)
                    .getFirst()
                : sr.getIdMapping()
                    .get(id)
                    .getSecond();


            CellMap lhsValue = cellMap.project(fd.getLeftHandSide());
            Cell<?> rhsValue = cellMap.get(fd.getRhsAttribute());

            if(lhsValues.get(lhsValue) == null)
                lhsValues.put(lhsValue, new HashMap<>());

            if(lhsValues.get(lhsValue).get(rhsValue) == null)
                lhsValues.get(lhsValue).put(rhsValue, new HashSet<>());

            lhsValues
                .get(lhsValue)
                .get(rhsValue)
                .add(id);
        }

        for(CellMap lhs: lhsValues.keySet())
        {
            Map<Cell<?>, Set<Long>> rhsValueMap = lhsValues.get(lhs);

            if(rhsValueMap.size() <= 1)
                continue;

            //Find highest occurrence
            int maxOccurence = rhsValueMap
                .values()
                .stream()
                .mapToInt(set -> set.size())
                .max()
                .getAsInt();

            //Find element with highest occurence
            Object mv = rhsValueMap
                .entrySet()
                .stream()
                .filter(e -> e.getValue().size() == maxOccurence)
                .map(e -> e.getKey())
                .findFirst()
                .get();

            //Tuples attached to other values are added to violating tuples
            rhsValueMap
                .entrySet()
                .stream()
                .filter(e -> !e.getKey().equals(mv))
                .map(e -> e.getValue())
                .forEach(set -> viol.addAll(set));
        }
        
        return viol;
    }
    
}
