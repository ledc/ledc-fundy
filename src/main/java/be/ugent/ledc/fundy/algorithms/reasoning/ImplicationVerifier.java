package be.ugent.ledc.fundy.algorithms.reasoning;

import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.datastructures.FD;

/**
 * A helper class that checks if a given FD is implied by a set of other FDs.
 * @author abronsel
 */
public class ImplicationVerifier
{
    public static boolean isImplied(FD target, RuleSet<Dataset, FD> ruleset)
    {
        //To check if X -> a is implied
        return new AttributeClosureGenerator(ruleset)
            .generate(target.getLeftHandSide()) //Computed X+: closure of X
            .containsAll(target.getRightHandSide());//Check if RHS \subseteq X+
    }
}
