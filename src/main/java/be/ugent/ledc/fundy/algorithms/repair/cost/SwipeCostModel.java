package be.ugent.ledc.fundy.algorithms.repair.cost;

import be.ugent.ledc.core.cost.ConstantCostFunction;
import be.ugent.ledc.core.cost.CostFunction;
import be.ugent.ledc.core.cost.CostModel;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.datastructures.FD;
import java.util.Map;
import java.util.stream.Collectors;

public class SwipeCostModel extends CostModel<CostFunction<?>>
{

    public SwipeCostModel(Map<String, CostFunction<?>> costFunctions)
    {
        super(costFunctions);
    }

    /**
     * The default cost function is a constant cost functions, which is in line
     * with the previous default behavior of "majority vote".
     * @param ruleset 
     */
    public SwipeCostModel(RuleSet<Dataset, FD> ruleset)
    {
        this(ruleset
            .stream()
            .flatMap(fd -> fd.getInvolvedAttributes().stream())
            .distinct()
            .collect(Collectors.toMap(
                a->a,
                a->new ConstantCostFunction<>()))
        );
    }
    
}
