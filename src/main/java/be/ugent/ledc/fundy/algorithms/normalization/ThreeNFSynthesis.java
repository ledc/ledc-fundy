package be.ugent.ledc.fundy.algorithms.normalization;

import be.ugent.ledc.core.binding.jdbc.schema.Key;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.reasoning.CandidateKeyGenerator;
import be.ugent.ledc.fundy.algorithms.reasoning.MinimalCoverGenerator;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.NormalisationException;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Implements the 3NF synthesis algorithm by decomposing a schema into 
 * a set of sub-schemata such that each subschema is in 3NF. This decomposition
 * is guaranteed to be lossless (i.e., satisfies the lossless join property)
 * and dependency preserving.
 * @author abronsel
 */
public class ThreeNFSynthesis extends AbstractNormalizer
{
    public ThreeNFSynthesis()
    {
        super(NormalForm._3NF);
    }  
    
    @Override
    /**
     * Decomposes the given schema into a set of sub-schemata such that each subschema is in 3NF,
     * accounting for the FDs given in the ruleset.
     * 
     * The decomposition is guaranteed to be lossless (i.e., satisfies the lossless join property)
     * and dependency preserving. The output is presented as a map of each sub-schema
     * to the projection of the original ruleset over this sub-schema.
     * 
     * @author abronsel
     * @param schema
     * @param ruleset
     * @return A map of sub-schemata to the projections of the original ruleset over these sub-schemata.
     * @throws be.ugent.ledc.fundy.datastructures.FDException
    */
    public Map<TableSchema, RuleSet<Dataset, FD>> decompose(TableSchema schema, RuleSet<Dataset, FD> ruleset) throws NormalisationException
    {
        //Init decomposition
        Map<TableSchema, RuleSet<Dataset, FD>> decomposition = new HashMap<>();
        add(decomposition, schema, ruleset);
        
        if(!normalized(schema, ruleset))
        {
            //Clear
            decomposition.clear();
            
            //Generate a minimal cover
            RuleSet<Dataset, SimpleFD> cover = new MinimalCoverGenerator().generate(ruleset);
            
            //Generate candidate keys
            Set<Key> candidateKeys = new CandidateKeyGenerator(schema).generate(ruleset);
            
            Map<Set<String>, List<SimpleFD>> groupedFDs = cover
                .stream()
                .collect(Collectors.groupingBy(SimpleFD::getLeftHandSide));

            for(Set<String> lhs: groupedFDs.keySet())
            {   
                Set<String> projection = Stream
                .concat(
                    lhs.stream(),
                    groupedFDs
                        .get(lhs)
                        .stream()
                        .flatMap(fd -> fd.getRightHandSide().stream())
                )
                .collect(Collectors.toSet());

                TableSchema proj = projectSchema(
                    schema,
                    projection,
                    new Key(lhs),
                    schema
                        .getName()
                        .concat("_")
                        .concat(""+decomposition.size())
                );
                
                add(decomposition,
                    proj,
                    new RuleSet<>(cover
                        .stream()
                        .filter(sfd -> sfd.allInvolved(proj.getAttributeNames()))
                        .map(sfd -> new FD(sfd.getLeftHandSide(),sfd.getRightHandSide()))
                        .collect(Collectors.toSet())
                    )
                );
            }
            
            if(decomposition
                .keySet()
                .stream()
                .noneMatch(ts -> candidateKeys
                    .stream()
                    .anyMatch(key -> ts.getAttributeNames().containsAll(key)))
            )
            {
                Key key = candidateKeys
                    .stream()
                    .findFirst()
                    .get();
                
                TableSchema proj = projectSchema(
                    schema,
                    key,
                    key,
                    schema
                        .getName()
                        .concat("_")
                        .concat(""+decomposition.size())
                );
                
                add(decomposition, proj, new RuleSet<>());
            }
            
        }
        
        return decomposition;
    }
    
    private void add(Map<TableSchema, RuleSet<Dataset, FD>> decomposition, TableSchema schema, RuleSet<Dataset, FD> rules)
    {   
        if(schema.keySet().isEmpty())
            return;
        
        decomposition
            .keySet()
            .removeIf(dc -> schema.keySet().containsAll(dc.keySet()));
        
        if(decomposition
            .keySet()
            .stream()
            .noneMatch(ts -> ts.keySet().containsAll(schema.keySet())))
        {
            decomposition.put(schema, rules);
        }
    }
}
