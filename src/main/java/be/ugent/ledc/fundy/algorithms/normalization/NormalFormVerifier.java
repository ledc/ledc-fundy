package be.ugent.ledc.fundy.algorithms.normalization;

import be.ugent.ledc.core.binding.jdbc.schema.Key;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.reasoning.CandidateKeyGenerator;
import be.ugent.ledc.fundy.algorithms.reasoning.MinimalCoverGenerator;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.NormalisationException;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import java.util.Set;
import java.util.stream.Collectors;

public class NormalFormVerifier
{
    public boolean hasNormalForm(TableSchema tableSchema, RuleSet<Dataset, FD> ruleset, NormalForm normalForm) throws NormalisationException
    {
        //Generate all candidate keys
        Set<Key> candidateKeys = new CandidateKeyGenerator(tableSchema).generate(ruleset);
        
        //Convert ruleset
        RuleSet<Dataset, SimpleFD> minimalCover = new MinimalCoverGenerator().generate(ruleset);
        
        if(normalForm.getLevel() >= NormalForm._2NF.getLevel()
            && !partialKeyCheck(candidateKeys, minimalCover))
        {
            return false;
        }
        if(normalForm.getLevel() >= NormalForm._3NF.getLevel()
            && !transitiveDependencyCheck(candidateKeys, minimalCover))
        {
            return false;
        }
        
        if(normalForm == NormalForm.BCNF)
            return bcnfCheck(candidateKeys, minimalCover);
        
        return true;
    }

    public Set<FD> getBCNFViolations(TableSchema tableSchema, RuleSet<Dataset, FD> ruleset) throws NormalisationException
    {
        return getBCNFViolations(
            new CandidateKeyGenerator(tableSchema).generate(ruleset),
            new MinimalCoverGenerator().generate(ruleset)
        );
    }
    
    private Set<FD> getBCNFViolations(Set<Key> candidateKeys, RuleSet<Dataset, SimpleFD> minimalCover)
    {
        //In the check below, we don't check for trivial FDs as those would
        //already have been removed by the minimal cover generator.
        return minimalCover
            .stream()
            .filter(fd -> candidateKeys //For each FD
                .stream()
                .noneMatch(key -> fd //LHS must be a key or superkey
                    .getLeftHandSide()
                    .containsAll(key))
            ).collect(Collectors.toSet());

    }
    
    private boolean partialKeyCheck(Set<Key> candidateKeys, RuleSet<Dataset, SimpleFD> minimalCover)
    {
        //Prime attributes
        Set<String> primeAttributes = candidateKeys
            .stream()
            .flatMap(key -> key.stream())
            .collect(Collectors.toSet());
        
        Set<SimpleFD> violations = minimalCover
            .stream()
            .filter(fd -> !primeAttributes.contains(fd.getRhsAttribute())
                        && candidateKeys
                        .stream()
                        .anyMatch(key -> key.containsAll(fd.getLeftHandSide())
                            && key.size() > fd.getLeftHandSide().size()))
            .collect(Collectors.toSet());
        
//        System.out.println("2NF violations:");
//        System.out.println(violations);
        return violations.isEmpty();
    }

    private boolean transitiveDependencyCheck(Set<Key> candidateKeys, RuleSet<Dataset, SimpleFD> minimalCover)
    {
        //Prime attributes
        Set<String> primeAttributes = candidateKeys
            .stream()
            .flatMap(key -> key.stream())
            .collect(Collectors.toSet());
        
        Set<SimpleFD> violations = minimalCover
            .stream()
            .filter(fd ->
                !primeAttributes.contains(fd.getRhsAttribute())
                && fd.getLeftHandSide()
                    .stream()
                    .anyMatch(a -> !primeAttributes.contains(a)))
            .collect(Collectors.toSet());
        
//        System.out.println("3NF violations:");
//        System.out.println(violations);
        
        return violations.isEmpty();
        
    }

    private boolean bcnfCheck(Set<Key> candidateKeys, RuleSet<Dataset, SimpleFD> minimalCover)
    {       
        return getBCNFViolations(candidateKeys, minimalCover).isEmpty();
    }
}
