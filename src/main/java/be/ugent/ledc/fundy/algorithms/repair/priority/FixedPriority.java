package be.ugent.ledc.fundy.algorithms.repair.priority;

import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.repair.SwipeRepair;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

/**
 * A Priority manager where the priority is predetermined and fixed. Can be
 * used to encode a desired priority model based on human intervention.
 * @author abronsel
 */
public class FixedPriority implements PriorityManager
{
    private final List<String> reliabilityOrder;

    public FixedPriority(List<String> reliabilityOrder)
    {
        this.reliabilityOrder = reliabilityOrder;
    }
    
    @Override
    public List<String> createPriority(SwipeRepair sr, RuleSet<Dataset, SimpleFD> fds, Set<String> repairClass)
    {
        List<String> priority = new ArrayList<>();
        
        //Attributes for which we don't know reliability are added first
        repairClass
            .stream()
            .filter(a -> !reliabilityOrder.contains(a))
            .forEach(priority::add);

        repairClass
            .stream()
            .filter(a -> reliabilityOrder.contains(a))
            .sorted(Comparator.comparing(reliabilityOrder::indexOf).reversed())
            .forEach(priority::add);
        
        
        return priority;
    }
    
}
