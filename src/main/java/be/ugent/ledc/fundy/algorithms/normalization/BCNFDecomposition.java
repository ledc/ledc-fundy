package be.ugent.ledc.fundy.algorithms.normalization;

import be.ugent.ledc.core.binding.jdbc.schema.Key;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.algorithms.reasoning.CandidateKeyGenerator;
import be.ugent.ledc.fundy.algorithms.reasoning.MinimalDeterminantFinder;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.NormalisationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Implements the BCNF decomposition algorithm by decomposing a schema into 
 * a set of sub-schemata such that each subschema is in BCNF. This decomposition
 * is guaranteed to be lossless (i.e., satisfies the lossless join property) but it 
 * is not necessarily dependency preserving.
 * @author abronsel
 */
public class BCNFDecomposition extends AbstractNormalizer
{
    public BCNFDecomposition()
    {
        super(NormalForm.BCNF);
    }
    
    /**
     * Decomposes the given schema into a set of sub-schemata such that each subschema is in BCNF,
     * accounting for the FDs given in the ruleset.
     * 
     * The decomposition is guaranteed to be lossless (i.e., satisfies the lossless join property)
     * but it is not necessarily dependency preserving. To that extent, the output is presented
     * as a map that maps each sub-schema to the projection of the original ruleset over the sub-schema.
     * 
     * @author abronsel
     * @param schema
     * @param ruleset
     * @return A map of sub-schemata to the projections of the original ruleset over these sub-schemata.
     * @throws be.ugent.ledc.fundy.datastructures.NormalisationException
    */
    @Override
    public Map<TableSchema, RuleSet<Dataset, FD>> decompose(TableSchema schema, RuleSet<Dataset, FD> ruleset) throws NormalisationException
    {
        //Collect all attributes
        RuleSet<Dataset, FD> transformed = new RuleSet<>(
            schema
            .getAttributeNames()
            .stream()
            .flatMap(target -> MinimalDeterminantFinder
                .findMinimalDeterminants(ruleset, target)
                .stream()
                .map(md -> new FD(md, SetOperations.set(target)))
            )
            .collect(Collectors.toSet())
        );
        
        //Init decomposition
        Map<TableSchema, RuleSet<Dataset, FD>> decomposition = new HashMap<>();
        decomposition.put(schema, transformed);
        
        String name = schema.getName();
        
        //Continue as long as not all schemas are in BCNF
        while(!normalized(decomposition))
        {
            //Get a violating schema
            TableSchema violatingSchema = getViolatingSchema(decomposition);
            
            //Get a violation
            FD violatingFD = getVerifier().getBCNFViolations(
                violatingSchema,
                decomposition.get(violatingSchema)
            )
            .stream()
            .findFirst()
            .orElseThrow(() -> new NormalisationException("Unexpected error in BCNF decomposition. Violating schema "
                    + violatingSchema.getName()
                    + " does not produce violating FDs."));
            
            decompose(decomposition, violatingSchema, violatingFD, name);
        }
        
        return decomposition;
    }
    
    private TableSchema getViolatingSchema(Map<TableSchema, RuleSet<Dataset, FD>> decomposition) throws NormalisationException
    {         
        for(TableSchema schema: decomposition.keySet())
        {
            if(!getVerifier().hasNormalForm(schema, decomposition.get(schema), NormalForm.BCNF))
                return schema;
        }
        return null;
    }

    private void decompose(Map<TableSchema, RuleSet<Dataset, FD>> decomposition, TableSchema violatingSchema, FD violatingFD, String name) throws NormalisationException
    {
        Set<String> leftAttributes = violatingFD.getInvolvedAttributes();
        Set<String> rightAttributes = Stream.concat(
            violatingFD
                .getLeftHandSide()
                .stream(),
            violatingSchema
                .getAttributeNames()
                .stream()
                .filter(a -> !violatingFD.getInvolvedAttributes().contains(a))
        )
        .collect(Collectors.toSet());
        
        TableSchema left = projectSchema(
            violatingSchema,
            leftAttributes,
            new Key(violatingFD.getLeftHandSide()),
            name.concat("_".concat(String.valueOf(decomposition.size()))));
        
        TableSchema right = projectSchema(
            violatingSchema,
            rightAttributes,
            new Key(),
            name.concat("_".concat(String.valueOf(decomposition.size()+1))));
        
        RuleSet<Dataset, FD> leftRules = decomposition
            .get(violatingSchema)
            .project(leftAttributes);
        RuleSet<Dataset, FD> rightRules = decomposition
            .get(violatingSchema)
            .project(rightAttributes);
        
        Set<Key> rightKeys = new CandidateKeyGenerator(right).generate(rightRules);
        
        right.setPrimaryKey(rightKeys.stream().findFirst().get());
        
        decomposition.put(left, leftRules);
        decomposition.put(right,rightRules);
        
        decomposition.remove(violatingSchema);
    }
}
