package be.ugent.ledc.fundy.algorithms.detection;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.util.LexicographicComparator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Detector
{
    public static Map<FD, Set<FDViolation>> detectErrors(Dataset data, RuleSet<Dataset, FD> fds)
    {
        //Initialize mapping of violations
        Map<FD, Set<FDViolation>> violationMap = new HashMap<>();
        
        LexicographicComparator<String> lgc = new LexicographicComparator<>();
        
        //Sort FDs according to LHS
        List<FD> sortedFDs = fds
            .stream()
            .sorted((fd1,fd2) -> lgc
                .compare(
                    fd1.getLeftHandSide() //Sort LHS 1 alphabetically
                        .stream()
                        .sorted()
                        .collect(Collectors.toList()),
                    fd2.getLeftHandSide() //Sort LHS 2 alphabetically
                        .stream()
                        .sorted()
                        .collect(Collectors.toList())))
            .collect(Collectors.toList());
        
        //Iterate over sorted FDs
        for(int i=0; i<sortedFDs.size(); i++)
        {
            FD next = sortedFDs.get(i);
            
            violationMap.put(next, new HashSet<>());
            
            Set<String> lhs = next.getLeftHandSide();
            
            //Check if data needs sorting
            if(i == 0
            || !Objects.equals(
                    sortedFDs.get(i-1).getLeftHandSide(),
                    next.getLeftHandSide())
            )
            {
                //Sort objects by left hand side
                data = data
                    .sort(DataObject
                        .getProjectionComparator(new ArrayList<>(next.getLeftHandSide())
                    ));
            }
            
            int from = 0;
                       
            //Scan the sorted data
            while(from < data.getSize())
            {
                DataObject seed = data
                    .getDataObjects()
                    .get(from);
                
                int to = from + 1;
                
                while(to < data.getSize() && seed.project(lhs).equals(data.getDataObjects().get(to).project(lhs)))
                    to++;
               
                //Get objects
                List<DataObject> objects = data
                    .getDataObjects()
                    .subList(from, to); //Sublist has exclusive end index
                    
                if(objects.size() == 1 || objects
                .stream()
                .anyMatch(o -> next.getLeftHandSide()
                        .stream()
                        .anyMatch(a -> o.get(a) == null)))
                {
                    from = to;
                    continue;
                }
                    
                Map<DataObject, Long> valueCounts = objects    
                        .stream()
                        .map(o -> o.project(next.getRightHandSide()))
                        .collect(Collectors.groupingBy(
                                Function.identity(),
                                Collectors.counting()));

                
                if(valueCounts.size() != 1)
                {
                    violationMap.get(next).add(new FDViolation(seed.project(lhs), valueCounts));
                }

                from = to;
            }
        }
        
        violationMap.entrySet().removeIf(e -> e.getValue().isEmpty());
        
        return violationMap;
    }
    
    public static Map<DataObject, Set<FD>> buildViolationMap(Dataset data, RuleSet<Dataset, FD> fds)
    {
        //Initialize mapping of violations
        Map<DataObject, Set<FD>> violationMap = new HashMap<>();
        
        LexicographicComparator<String> lgc = new LexicographicComparator<>();
        
        //Sort FDs according to LHS
        List<FD> sortedFDs = fds
            .stream()
            .sorted((fd1,fd2) -> lgc
                .compare(
                    fd1.getLeftHandSide() //Sort LHS 1 alphabetically
                        .stream()
                        .sorted()
                        .collect(Collectors.toList()),
                    fd2.getLeftHandSide() //Sort LHS 2 alphabetically
                        .stream()
                        .sorted()
                        .collect(Collectors.toList())))
            .collect(Collectors.toList());
        
        //Iterate over sorted FDs
        for(int i=0; i<sortedFDs.size(); i++)
        {
            FD next = sortedFDs.get(i);
            
            Set<String> lhs = next.getLeftHandSide();
            
            //Check if data needs sorting
            if(i == 0
            || !Objects.equals(
                    sortedFDs.get(i-1).getLeftHandSide(),
                    next.getLeftHandSide())
            )
            {
                //Sort objects by left hand side
                data = data
                    .sort(DataObject
                        .getProjectionComparator(new ArrayList<>(next.getLeftHandSide())
                    ));
            }

            //Scan the sorted data
            for(int from=0; from<data.getSize() - 1; from++)
            {
                DataObject o1 = data.getDataObjects().get(from);
                DataObject o2 = data.getDataObjects().get(from + 1);
                
                //If LHS is the same and RHS differs, we have a violation
                if(o1.project(lhs).equals(o2.project(lhs))
                && !o1.project(next.getRightHandSide()).equals(o2.project(next.getRightHandSide())))
                {
                    violationMap.merge(
                        o1,
                        SetOperations.set(next),
                        SetOperations::union);
                    
                    violationMap.merge(
                        o2,
                        SetOperations.set(next),
                        SetOperations::union);
                }
            }
        }
        
        return violationMap;
    }
}
