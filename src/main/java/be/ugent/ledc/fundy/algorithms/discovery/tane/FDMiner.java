package be.ugent.ledc.fundy.algorithms.discovery.tane;

import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import java.util.List;

/**
 * An interface that identifies algorithms that look for valid FDs in a given dataset.
 * @author abronsel
 */
public interface FDMiner
{
    public List<SimpleFD> discover(Dataset dataset) throws DependencyMiningException;
}
