package be.ugent.ledc.fundy.algorithms.repair;

import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.relation.BinaryRelation;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.algorithms.reasoning.MinimalCoverGenerator;
import be.ugent.ledc.fundy.datastructures.DisjointSetForest;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A partition generator that computes, for a given set of FDs, a partition that
 * is sequentially forward repairable.
 * @author abronsel
 */
public class PartitionGenerator
{
    /**
     * Generates a SwipePartition of attributes that is sequentially forward
     * repairable.
     * @param ruleset
     * @return
     * @throws RepairException 
     */
    public static SwipePartition generate(RuleSet<Dataset, SimpleFD> ruleset) throws RepairException
    {       
        //Generate a minimal cover of FDs
        RuleSet<Dataset, SimpleFD> minCover = new MinimalCoverGenerator().generate(ruleset);
        
        //Convert that minimal cover in a set of statements regarding
        //the order of attributes
        Set<PreOrderStatement> statements = convert(minCover);
                       
//        statements.stream().forEach(System.out::println);
        
        //Initialize the class list: equivalent attributes are in the same class
        Set<Set<String>> classes = buildClasses(statements);
        
        //In order to sort the groups of attributes, we can use Kahn's algorithm
        //to produce a topological sort
        
        List<Set<String>> classList = new ArrayList<>();
        
        //Statements that mark equivalences are removed.
        statements.removeIf(stmt -> classes
            .stream()
            .anyMatch(cl ->
                cl.contains(stmt.getLeft())
            &&  cl.contains(stmt.getRight())));
        
        //Set of start classes: classes that must be in front because no attribute
        //appears to the right
        List<Set<String>> frontier = classes
            .stream()
            .filter(cls -> statements
                .stream()
                .allMatch(stmt -> !cls.contains(stmt.getRight())))
            .collect(Collectors.toList());
        
        //Remove classes in start list from the set of classes
        classes.removeIf(cl -> frontier.contains(cl));
        
        while(!frontier.isEmpty())
        {
            //Remove next class from start
            Set<String> nextClass = frontier.remove(0);
            
            //Add to class list
            classList.add(nextClass);
            
            //Clean statements
            statements.removeIf(stmt -> nextClass.contains(stmt.getLeft()));
            
            //Each class that after cleansing of statements has no member in
            //the right of any statement, is added to the frontier.
            classes
                .stream()
                .filter(cl -> statements
                    .stream()
                    .allMatch(stmt -> !cl.contains(stmt.getRight())))
                .forEach(frontier::add);
            
            //Remove from the remaining classes any class that is now in the frontier.
            classes.removeIf(cl -> frontier.contains(cl));
        }

        SwipePartition swipePartition = new SwipePartition();
        
        for(Set<String> cl: classList)
        {
            swipePartition.addRepairClass(cl);
        }
        
        return swipePartition;
    }
    
    /**
     * Generates a SwipePartition of attributes that is sequentially forward
     * repairable and additionally adds classes for attributes constrained by a
     * regex.
     * @param ruleset
     * @return
     * @throws RepairException 
     */
    public static SwipePartition generate(RuleSet<Dataset, SimpleFD> ruleset, Map<String,String> regexes) throws RepairException
    {       
        SwipePartition swipePartition = generate(ruleset);
        
        regexes
            .keySet()
            .stream()
            .filter(a -> ruleset
                .stream()
                .noneMatch(fd -> fd.involves(a)))
            .map(a -> SetOperations.set(a))
            .forEach(swipePartition::addRepairClass);
        
        return swipePartition;
    }
    
    /**
     * Converts a minimal cover of FDs into a set of order statements
     * that is closed under transitivity.
    */
    private static Set<PreOrderStatement> convert(RuleSet<Dataset, SimpleFD> ruleset)
    {
        //Build initial set of statements
        Set<PreOrderStatement> statements = build(ruleset);
        
        //Compute the transitive closure of the set of statements
        Set<String> attributes = ruleset
        .stream()
        .flatMap(fd -> fd
            .getInvolvedAttributes()
            .stream())
        .sorted()
        .distinct()
        .collect(Collectors.toSet());
        
//        //Deep copy the matrix
//        boolean[][] closedMatrix = new boolean[attributes.size()][attributes.size()];

        BinaryRelation<String> rel = new BinaryRelation<>(attributes);
//        
//        //Ensure reflexivity
//        for(int i = 0; i<attributes.size(); i++)
//        {
//            closedMatrix[i][i] = true;
//        }

        for(String a: attributes)
            rel.add(a,a);
        
        for(PreOrderStatement os : statements)
        {
//            closedMatrix //Indicate left BEFORE right
//                [attributes.indexOf(os.getLeft())]
//                [attributes.indexOf(os.getRight())]
//                = true;
            rel.add(os.getLeft(),os.getRight());
        }

        BinaryRelation<String> closedRel = rel.transitiveClosure();

           
        Set<PreOrderStatement> closed = new HashSet<>();
        
        for(String a: attributes)
        {
            for(String b: attributes)
            {
                //if(closedMatrix[i][j] && i != j)
                if(closedRel.contains(a, b) && !a.equals(b))
                    closed.add(new PreOrderStatement(a, b));
            }
        }
        
        //Return
        return closed;
    }
    
    private static Set<PreOrderStatement> build(RuleSet<Dataset, SimpleFD> ruleset)
    {
        Set<PreOrderStatement> statements = new HashSet<>();
                
        for(SimpleFD fd: ruleset)
        {
            for(String a: fd.getLeftHandSide())
            {
                statements.add(new PreOrderStatement(a, fd.getRhsAttribute()));
            }
        }
        
        //Return
        return statements;
    }

    /**
     * Builds classes by grouping attributes that are equivalent under the given
     * pre-order implied by the FDs.
     * @param statements
     * @return 
     */
    private static Set<Set<String>> buildClasses(Set<PreOrderStatement> statements)
    {
        DisjointSetForest<String> dsf = new DisjointSetForest<>();
        
        //Get all attributes
        List<String> attributes = statements
            .stream()
            .flatMap(stm -> Stream.of(stm.getLeft(), stm.getRight()))
            .sorted()
            .distinct()
            .collect(Collectors.toList());
        
        //Populate the forest with singleton classes
        attributes
            .stream()
            .forEach(dsf::add);
                
        //Make attributes equivalent
        for(PreOrderStatement pos: statements)
        {
            if(statements.contains(new PreOrderStatement(pos.getRight(), pos.getLeft())))
            {
                dsf.merge(pos.getLeft(), pos.getRight());
            }
        }
        
        return new HashSet<>(dsf.build());
    }
    
    private static class PreOrderStatement
    {
        private final String left;
        
        private final String right;

        public PreOrderStatement(String left, String right)
        {
            this.left = left;
            this.right = right;
        }

        public String getLeft()
        {
            return left;
        }

        public String getRight()
        {
            return right;
        }

        @Override
        public int hashCode()
        {
            int hash = 3;
            hash = 59 * hash + Objects.hashCode(this.left);
            hash = 59 * hash + Objects.hashCode(this.right);
            return hash;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (this == obj)
            {
                return true;
            }
            if (obj == null)
            {
                return false;
            }
            if (getClass() != obj.getClass())
            {
                return false;
            }
            final PreOrderStatement other = (PreOrderStatement) obj;
            if (!Objects.equals(this.left, other.left))
            {
                return false;
            }
            if (!Objects.equals(this.right, other.right))
            {
                return false;
            }
            return true;
        }

        @Override
        public String toString()
        {
            return left + " <= " + right;
        }
    }
}
