package be.ugent.ledc.fundy.algorithms.reasoning;

import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class MinimalCoverGenerator
{  
    private final boolean removeRedundant;
    
    private final boolean reduce;

    public MinimalCoverGenerator(boolean removeRedundant, boolean reduce)
    {
        this.removeRedundant = removeRedundant;
        this.reduce = reduce;
    }
    
    public MinimalCoverGenerator()
    {
        this(true,true);
    }
    
    public RuleSet<Dataset, SimpleFD> generate(RuleSet<Dataset, ? extends FD> ruleset)
    {
        //Step 1: convert to simple FDs
        Set<FD> simplified = new HashSet<>();
        
        for(FD fd: ruleset)
        {
            for(String a: fd.getRightHandSide())
            {
                //Avoid trivial FDs
                if(!fd.getLeftHandSide().contains(a))
                {
                    simplified.add(new SimpleFD(fd.getLeftHandSide(), a));
                }
            }
        }
        
        if(removeRedundant)
        {
            Set<FD> redundantFDs = new HashSet<>();

            //Step 2: remove redundant FDs
            for(FD target: simplified)
            {
                Set<FD> subset = new HashSet<>();

                subset.addAll(simplified);
                subset.removeAll(redundantFDs);
                subset.remove(target);

                //If the FD is implied by the lesser set, it is redundant
                if(ImplicationVerifier.isImplied(target, new RuleSet<>(subset)))
                    redundantFDs.add(target);
            }

            simplified.removeAll(redundantFDs);
        }
        
        if(reduce)
        {
            //Step 3: reduce FDs        
            for(FD target: simplified)
            {
                Set<String> extraneous = new HashSet<>();

                //Find extraneous attributes;
                for(String a: target.getLeftHandSide())
                {                
                    Set<String> lhs = new HashSet<>();
                    lhs.addAll(target.getLeftHandSide());
                    lhs.removeAll(extraneous);
                    lhs.remove(a);

                    //If the FD is implied by the lesser set, it is redundant
                    if(ImplicationVerifier.isImplied(
                        new SimpleFD(lhs, a),
                        new RuleSet<>(simplified))
                    )
                    {
                        extraneous.add(a);
                    }
                }

                if(!extraneous.isEmpty())
                {
                    target.getLeftHandSide().removeAll(extraneous);
                }
            }
        }
        
        return new RuleSet<>(simplified
            .stream()
            .map(fd -> (SimpleFD)fd)
            .collect(Collectors.toSet()));
        
    }
}
