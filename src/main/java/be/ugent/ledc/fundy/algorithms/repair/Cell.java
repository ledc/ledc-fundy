package be.ugent.ledc.fundy.algorithms.repair;

import java.util.Objects;

/**
 * A helper class that holds either a constant value or a numbered variable.
 * @author abronsel
 * @param <T> Type of data value held by this Cell
 */
public class Cell<T>
{
    private T value;
    
    private long cellNumber;

    public Cell(T constant, long cellNumber)
    {
        this.value = constant;
        this.cellNumber = cellNumber;
    }

    public T getValue() {
        return value;
    }

    public long getCellNumber()
    {
        return cellNumber;
    }

    public void setValue(T value)
    {
        this.value = value;
    }

    public void setCellNumber(int cellNumber)
    {
        this.cellNumber = cellNumber;
    }
    
    @Override
    public int hashCode()
    {
        return value != null
            ? 17 * 7 + Objects.hashCode(this.value)
            : 17 * 7 + (int)this.cellNumber;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final Cell<?> other = (Cell<?>) obj;
        
        //If both values are not null and equal, cells are equal
        if(this.value != null && other.getValue() != null && Objects.equals(this.value, other.getValue()))
            return true;
        
        //If both cells are null but have the same number, the values must be equal
        if(this.value == null && other.getValue() == null && this.cellNumber == other.getCellNumber())
            return true;
        
        //Any other case yields false
        return false;
    }

    @Override
    public String toString()
    {
        return value == null
            ?"lvar_" + cellNumber
            : value.toString();
    }
}
    
    

