package be.ugent.ledc.fundy.algorithms.reasoning;

import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.datastructures.FD;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This class represents a linear time algorithm to find the attribute closure X+
 * of some set of attributes X. The algorithm was presented in the paper
 * "Computational problems related to the design of normal form relational schemas"
 * by Catriel Beeri and Philip A. Bernstein and was published in
 * "ACM Transactions on Database Systems", Vol. 4, Issue 1, March 1979,  pp 30–59
 * See https://doi.org/10.1145/320064.320066
 * @author abronsel
 */
public class AttributeClosureGenerator
{
    private final RuleSet<Dataset, FD> ruleset;

    public AttributeClosureGenerator(RuleSet<Dataset, FD> ruleset)
    {
        this.ruleset = ruleset;
    }
    
    public Set<String> generate(Set<String> attributes)
    {
        //Step 1: Adding all given attributes to closure
        Set<String> closure = new HashSet<>(attributes);
        
        //Any attribute is a priori determined, is in the closure
        ruleset
            .stream()
            .filter(fd -> fd.getLeftHandSide().isEmpty())
            .flatMap(fd -> fd.getRightHandSide().stream())
            .forEach(closure::add);
        
        //Step 2: collect all attributes
        Set<String> involvedAttributes = ruleset
            .stream()
            .flatMap(fd -> fd.getInvolvedAttributes().stream())
            .collect(Collectors.toSet());
        
        involvedAttributes.addAll(attributes);
        
        //Step 3: create attribute list & counter
        Map<String, Set<FD>> attrList = involvedAttributes
            .stream()
            .collect(Collectors.toMap(
                a -> a,
                a -> new HashSet<>()));
        
        Map<FD, Integer> counter = new HashMap<>();
        
        //Step 4: 
        for(FD fd: ruleset)
        {
            for(String a: fd.getLeftHandSide())
            {
                counter.merge(fd, 1, Integer::sum);
                attrList.get(a).add(fd);
            }
        }
        
        //Step 5: set of new dependents
        LinkedList<String> toCheck = new LinkedList<>(attributes);
        
        //Main loop: O(|ruleset|)
        while(!toCheck.isEmpty())
        {
            //Get the next to check and remove it
            String nextToCheck = toCheck.pop();
            
            for(FD fd: attrList.get(nextToCheck))
            {
                counter.put(fd, counter.get(fd) - 1);
                
                if(counter.get(fd) == 0)
                {
                    for(String rhsAttribute: fd.getRightHandSide())
                    {
                        if(!closure.contains(rhsAttribute))
                        {
                            closure.add(rhsAttribute);
                            toCheck.push(rhsAttribute);
                        }
                    }
                }
            }
        }
        
        return closure;
    }
}
