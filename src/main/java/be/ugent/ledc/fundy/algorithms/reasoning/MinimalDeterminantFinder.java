package be.ugent.ledc.fundy.algorithms.reasoning;

import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.datastructures.FD;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Find all minimal determinants that determine a given target attribute.
 * @author abronsel
 */
public class MinimalDeterminantFinder
{   
    public static Set<Set<String>> findMinimalDeterminants(RuleSet<Dataset, FD> ruleset, String target)
    {       
        Set<String> attributes = ruleset
            .stream()
            .flatMap(fd -> fd.getInvolvedAttributes().stream())
            .filter(a -> !a.equals(target))
            .collect(Collectors.toSet());
        
        AttributeClosureGenerator acg = new AttributeClosureGenerator(ruleset);

        //Compose combinations to test
        Map<Set<String>, Set<String>> currentLevel = attributes
            .stream()
            .filter(a -> ruleset
                .stream()
                .anyMatch(fd -> fd.getLeftHandSide().contains(a)))
            .map(a -> SetOperations.set(a))
            .collect(Collectors.toMap(
                set -> set, 
                set -> acg.generate(set))
            );
        
        boolean stop = false;
        
        while(!stop)
        {
            //Generate next level
            Map<Set<String>, Set<String>> nextLevel = nextLevel(
                currentLevel,
                ruleset,
                acg,
                target,
                attributes);
            
            currentLevel.clear();
            currentLevel.putAll(nextLevel);
            
            //Clean
            currentLevel
            .keySet()
            .removeIf(minSet -> currentLevel
                .entrySet()
                .stream()
                .anyMatch(e -> !e.getKey().equals(minSet) // A different entry
                    && minSet.containsAll(e.getKey())   // minSet is a superset
                ));
            
            stop = currentLevel
                .values()
                .stream()
                .allMatch(v -> v.contains(target));
        }
               
        return currentLevel.keySet().stream().collect(Collectors.toSet());
    }

    private static Map<Set<String>, Set<String>> nextLevel(Map<Set<String>, Set<String>> currentLevel, RuleSet<Dataset, FD> ruleset, AttributeClosureGenerator acg, String target, Set<String> attributes)
    {
        Map<Set<String>, Set<String>> nextLevel = new HashMap<>();
        
        for(Set<String> candidate: currentLevel.keySet())
        {
            //If candidate is a minimal determinant, pass it to the next level
            if(currentLevel.get(candidate).contains(target))
            {
                nextLevel.put(candidate, currentLevel.get(candidate));
                continue;
            }
            
            Set<String> attributesToCombine = attributes
                .stream()
                .filter(a -> !currentLevel
                    .get(candidate)
                    .contains(a))
                .filter(a -> ruleset
                    .stream()
                    .anyMatch(fd -> fd.getLeftHandSide().contains(a)
                        && !currentLevel.get(candidate).containsAll(fd.getRightHandSide()))
                )
                .collect(Collectors.toSet());
            
            for(String a: attributesToCombine)
            {
                Set<String> newCandidate = new HashSet<>();
                newCandidate.addAll(candidate);
                newCandidate.add(a);
                
                nextLevel.put(newCandidate, acg.generate(newCandidate));
            }
        }
        
        return nextLevel;
    }
}
