package be.ugent.ledc.fundy.algorithms.normalization;

import be.ugent.ledc.core.binding.jdbc.schema.Key;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.reasoning.ImplicationVerifier;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.NormalisationException;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Models a normalization procedure that transforms a schema into a desired normal form.
 * @author abronsel
 */
public abstract class AbstractNormalizer implements Normalizer
{
    private final NormalFormVerifier verifier = new NormalFormVerifier();
    
    private final NormalForm normalForm;

    protected AbstractNormalizer(NormalForm normalForm)
    {
        this.normalForm = normalForm;
    }

    protected NormalFormVerifier getVerifier()
    {
        return verifier;
    }

    protected TableSchema projectSchema(TableSchema violatingSchema, Set<String> attributes, Key key, String name)
    {
        TableSchema projection = new TableSchema();
        projection.putAll(violatingSchema);
        projection
            .keySet()
            .removeIf(a -> !attributes.contains(a));
        
        projection.setLogicalType(violatingSchema.getLogicalType());
        projection.setType(violatingSchema.getType());
        projection.setReferences(violatingSchema
            .getReferences()
            .stream()
            .filter(r -> attributes.containsAll(r.keySet()))
            .collect(Collectors.toList())
        );
        projection.setPrimaryKey(key);
        projection.setName(name);
        
        return projection;
    }
    
    /**
     * Verifies that the given collection of sub-schemata all satisfy the normal form
     * of this normalizer, given the associated FDs.
     * @param decomposition
     * @return True if all sub-schemata satisfy the normal form of this normalizer.
     * @throws NormalisationException 
     */
    public boolean normalized(Map<TableSchema, RuleSet<Dataset, FD>> decomposition) throws NormalisationException
    {
        for(TableSchema schema: decomposition.keySet())
        {
            if(!normalized(schema, decomposition.get(schema)))
                return false;
        }
        return true;
    }
    
    /**
     * Verifies that the given schema satisfies the requested normal form,
     * given the associated FDs.
     * @param tableSchema
     * @param dependencies
     * @return True if the schema satisfies the normal form of this normalizer.
     * @throws NormalisationException 
     */
    public boolean normalized(TableSchema tableSchema, RuleSet<Dataset, FD> dependencies) throws NormalisationException
    {
        return getVerifier().hasNormalForm(
            tableSchema,
            dependencies,
        normalForm);
    }
    
    /**
     * Verifies that the set of FD projections is equivalent to an original set of FDs.
     * @param original
     * @param projections
     * @return True, if the set of FD projections is equivalent to the original set of FDs.
     */
    public static boolean isDependencyPreserving(RuleSet<Dataset, FD> original, Set<RuleSet<Dataset, FD>> projections)
    {
        RuleSet<Dataset, FD> union = new RuleSet<>(projections
            .stream()
            .flatMap(set -> set.stream())
            .collect(Collectors.toSet())
        );
        
        //The decomposition is dependency preserving if all original FDs
        //are implied by the union of projected FDs
        Set<FD> lostFDs = original
        .stream()
        .filter(fd -> !ImplicationVerifier.isImplied(fd, union))
        .collect(Collectors.toSet());
        
        return lostFDs.isEmpty();
    }
}
