package be.ugent.ledc.fundy.algorithms.discovery.tane;

import be.ugent.ledc.core.LedcException;

public class DependencyMiningException extends LedcException
{
    public DependencyMiningException(String message)
    {
        super(message);
    }

    public DependencyMiningException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DependencyMiningException(Throwable cause)
    {
        super(cause);
    }

    public DependencyMiningException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
