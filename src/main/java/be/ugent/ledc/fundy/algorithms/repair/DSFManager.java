package be.ugent.ledc.fundy.algorithms.repair;

import be.ugent.ledc.fundy.datastructures.DisjointSetForest;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DSFManager
{
    private final Map<String, DisjointSetForest<Long>> repairPartitions;
    
    public DSFManager()
    {
        this.repairPartitions = new HashMap<>();
    }
 
    public void init()
    {
        this.repairPartitions.clear();
    }
    
    public void update(SimpleFD fd, SwipeRepair sr)
    {
        if(repairPartitions.get(fd.getRhsAttribute()) == null)
            repairPartitions.put(fd.getRhsAttribute(), new DisjointSetForest<>());
        
        //Get the current forest for RHS of FD. If we previously repaired an FD
        //we start from the already existing partition.
        DisjointSetForest<Long> dsf = repairPartitions.get(fd.getRhsAttribute());

        //Initialize a mapping of determinants to roots
        Map<CellMap, Long> determinantRootNodes = new HashMap<>();
        
        //In order to repair an FD X -> a, we partition data on equivalent values for X
        for(Long id: sr.getIdMapping().keySet())
        {
            //Get determinant
            CellMap determinant = sr
                .getIdMapping()
                .get(id)
                .getSecond()
                .project(fd.getLeftHandSide());
            
            if(fd.getLeftHandSide().stream().anyMatch(a -> determinant.get(a) == null))
                throw new RuntimeException("Cells "
                    + determinant
                    + " must contain a cell for each LHS attribute of "
                    + fd
                );
                
            if(!dsf.contains(id))
            {
                dsf.add(id);   
            }
            
            if(!determinantRootNodes.containsKey(determinant))
            {
                determinantRootNodes.put(determinant, dsf.find(id));
            }
            //Else merge
            else
            {
                dsf.merge(id, determinantRootNodes.get(determinant));
                determinantRootNodes.put(determinant, dsf.find(id));
            }
            
            if(Swipe.VERBOSITY >= 6)
            {
                System.out.println("DSF after observing tid " + id);
                
                dsf.getParents()
                   .entrySet()
                   .stream()
                   .map(e -> "  Parent["+ e.getKey() + "]=" + e.getValue())
                   .forEach(System.out::println);
                
                System.out.println("");
            }
        }

    }

    public Collection<Set<Long>> build(String a)
    {
        return this.repairPartitions.get(a).build();
    }
}
