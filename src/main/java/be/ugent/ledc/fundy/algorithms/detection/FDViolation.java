package be.ugent.ledc.fundy.algorithms.detection;

import be.ugent.ledc.core.dataset.DataObject;
import java.util.Map;
import java.util.Objects;

public class FDViolation
{
    public final DataObject lhsValue;
    
    public final Map<DataObject, Long> rhsValues;

    public FDViolation(DataObject lhsValue, Map<DataObject, Long> rhsValues)
    {
        this.lhsValue = lhsValue;
        this.rhsValues = rhsValues;
    }

    public DataObject getLhsValue()
    {
        return lhsValue;
    }

    public Map<DataObject, Long> getRhsValues()
    {
        return rhsValues;
    }

    public boolean isStrongConflictFree()
    {
        return rhsValues
        .keySet()
        .stream()
        .anyMatch(dom -> rhsValues
            .keySet()
            .stream()
            .allMatch(subs -> dom.subsumes(subs)));
    }
    
    public boolean isWeakConflictFree()
    {
        return rhsValues
        .keySet()
        .stream()
        .allMatch(left -> rhsValues
            .keySet()
            .stream()
            .allMatch(right -> left.complements(right)
            ));
    
    }
    
    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.lhsValue);
        hash = 71 * hash + Objects.hashCode(this.rhsValues);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final FDViolation other = (FDViolation) obj;
        if (!Objects.equals(this.lhsValue, other.lhsValue))
        {
            return false;
        }
        if (!Objects.equals(this.rhsValues, other.rhsValues))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return lhsValue + " -> " + rhsValues;
    }
}
