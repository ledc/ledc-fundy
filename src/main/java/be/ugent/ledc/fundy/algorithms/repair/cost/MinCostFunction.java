package be.ugent.ledc.fundy.algorithms.repair.cost;

import be.ugent.ledc.core.cost.CostFunction;
import be.ugent.ledc.core.dataset.DataObject;
import java.util.Objects;

public class MinCostFunction<T extends Comparable<? super T>> implements CostFunction<T>
{
    public static int HIGH = 1000;
    
    @Override
    public int cost(T originalValue, T repairedValue, DataObject originalObject)
    {
        if(originalValue != null
        && repairedValue != null
        && Objects.equals(originalValue,repairedValue))
            return 0;
        
        if(repairedValue == null)
            return HIGH;
        
        if(originalValue == null)
            return 1;
        
        return originalValue.compareTo(repairedValue) > 0 ? 1 : HIGH;
    }
}
