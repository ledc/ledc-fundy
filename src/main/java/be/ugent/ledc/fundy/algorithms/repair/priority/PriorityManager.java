package be.ugent.ledc.fundy.algorithms.repair.priority;

import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.repair.SwipeRepair;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import java.util.List;
import java.util.Set;

public interface PriorityManager
{
    public List<String> createPriority(SwipeRepair sr, RuleSet<Dataset, SimpleFD> fds, Set<String> repairClass);
}
