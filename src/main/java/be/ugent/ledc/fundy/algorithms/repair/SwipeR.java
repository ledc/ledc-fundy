package be.ugent.ledc.fundy.algorithms.repair;

import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.cost.CostFunction;
import be.ugent.ledc.core.cost.CostModel;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.datastructures.Couple;
import be.ugent.ledc.core.datastructures.Multiset;
import be.ugent.ledc.core.datastructures.Pair;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.core.operators.UnitScore;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.fundy.algorithms.reasoning.MinimalCoverGenerator;
import be.ugent.ledc.fundy.algorithms.repair.priority.PriorityManager;
import be.ugent.ledc.fundy.algorithms.repair.priority.ReliabilityPriority;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * SwipeR is an extension of the Swipe algorithm where each attribute can be assigned
 * a regex to constrain the domain. In addition to satisfaction of the FDs, we now
 * also ensure that each value of an attribute matches the regex for that attribute
 * if defined.
 * 
 * @author abronsel
 */
public class SwipeR
{
    public static int VERBOSITY = 0;
    
    private final RuleSet<Dataset, SimpleFD> ruleset;
    
    private final CostModel<?> costModel;
    
    private static final Logger LOGGER = Logger.getLogger(SwipeR.class.getName());
    
    private final PriorityManager priorityManager;
    
    private SwipeRepair sr;
    
    private final DSFManager dsf;
    
    private final Map<String, String> regexes;
    
    private final boolean propagate;
    
    private final double rhsEntropyBound;
    
    /**
     * In case of regex enforcement, we can need additional string values
     * to replace dirty values with
     */
    private final Map<String, Map<String,Set<String>>> explainerMap = new HashMap<>();
    
    /**
     * Keeps track of which consolidations are preservative
     */
    private final Map<String, Boolean> preservationMap = new HashMap<>();
      
    public SwipeR(RuleSet<Dataset, FD> ruleset, CostModel<?> costModel, PriorityManager priorityManager, Map<String, String> regexes, boolean propagate, double rhsEntropyBound)
    {
        this.ruleset = new MinimalCoverGenerator().generate(ruleset);
        this.costModel = costModel;
        this.priorityManager = priorityManager;
        this.regexes = regexes;
        this.dsf = new DSFManager();
        this.propagate = propagate;
        this.rhsEntropyBound = rhsEntropyBound;
    }
    
    public SwipeR(RuleSet<Dataset, FD> ruleset, CostModel<?> costModel, PriorityManager priorityManager, Map<String, String> regexes, boolean propagate)
    {
        this(ruleset, costModel, priorityManager, regexes, propagate, 5.0);
    }
    
    public SwipeR(RuleSet<Dataset, FD> ruleset, CostModel<?> costModel, PriorityManager priorityManager, Map<String, String> regexes)
    {
        this(ruleset, costModel, priorityManager, regexes, true);
    }
    
    public SwipeR(RuleSet<Dataset, FD> ruleset, CostModel<?> costModel, Map<String, String> regexes)
    {
        this(ruleset, costModel, new ReliabilityPriority(), regexes, true);
    }
    
    public SwipeR(RuleSet<Dataset, FD> ruleset, CostModel<?> costModel, PriorityManager priorityManager)
    {
        this(ruleset, costModel, priorityManager, new HashMap<>(), true);
    }
    
    public SwipeR(RuleSet<Dataset, FD> ruleset, CostModel<?> costModel)
    {
        this(ruleset, costModel, new ReliabilityPriority());
    }
    
    /**
     * Produces a repair for the given dataset that satisfies all FDs by following the Swipe algorithm.
     * @param dataset
     * @return
     * @throws RepairException 
     */
    public Dataset repair(Dataset dataset) throws RepairException
    {        
        //Build a partition (account for regexes)
        SwipePartition partition = PartitionGenerator.generate(ruleset, regexes);
        
        if(VERBOSITY >= 1)
            LOGGER.log(Level.INFO, "Partition: {0}", partition);
                           
        if(!ruleset
        .stream()
        .allMatch(fd -> partition.forward(fd)))
        {
            throw new RepairException("Cannot swipe the data. "
                + "Cause: partition "
                + partition
                + " is not forward repairable");
        }
        
        //Maintain attributes visited so far
        Set<String> projection = new HashSet<>();
        
        //Clear the mapping with explanations
        explainerMap.clear();
        
        //Set all preservative indicators to true by default
        costModel
            .getAttributes()
            .stream()
            .forEach(a -> preservationMap.put(a,true));
        
        for(String a: costModel.getAttributes())
        {
            if(costModel.getCostFunction(a) == null)
                continue;
            
            if(regexes.get(a) == null)
               continue;
            
            explainerMap.put(a, new HashMap<>());
            
            CostFunction<String> cf = costModel.getCostFunction(a);
            
            for(DataObject o: dataset)
            {
                String v = o.getString(a);
                
                if(v == null)
                    continue;
                
                Set<String> explanations = cf
                    .alternatives(v, o)
                    .stream()
                    .filter(expl -> expl == null || expl.matches(regexes.get(a)))
                    .collect(Collectors.toSet());
                
                explainerMap.get(a).put(v, explanations);
                
                if(!explanations.isEmpty())
                {
                    preservationMap.put(a, false);
                }
            }
        }
        
        //Construct a Swipe repair object from the dirty data
        sr = new SwipeRepair(
            dataset,
            ruleset
                .stream()
                .flatMap(fd -> fd.getInvolvedAttributes().stream())
                .collect(Collectors.toSet())
        );
                
        //Iterate over repair classes
        for(Set<String> rc: partition)
        {
            //Update projection
            projection.addAll(rc);
            
            //Get projection on FDs
            RuleSet<Dataset, SimpleFD> projectedRuleset = ruleset.project(projection);
            
            //Check if FDs should be repaired
            if(projectedRuleset
                .stream()
                .allMatch(rule -> !rc.contains(rule.getRhsAttribute()))
            )
            {
                if(VERBOSITY >= 1)
                {
                    LOGGER.log(Level.INFO, "Checking regex repair for {0}", rc);
                }
                repairRegexOnly(rc);
                
                if(VERBOSITY >= 1)
                {
                    LOGGER.log(Level.INFO, "Pre-cleaning for {0}", rc);
                }
                preClean(rc.stream().findFirst().get());
            }
            else
            {
                //Make changes to attributes in rc to fix all violations in
                repairForClass(projectedRuleset, rc);
            }
        }

        //Return clean dataset
        return new SimpleDataset(sr.getRepair())
            .inverseProject(SwipeRepair.LEDC_KEY);
        
    }

    /**
     * Repairs the data for attributes in a repair class (a group of attributes repaired jointly)
     * This means that all FDs in the projectedRuleset will be satisfied
     * after this step and only attributes present in the repair class will be modified.
     * @param projectedRuleset
     * @param repairClass
     * @return
     * @throws FDException 
     */
    private void repairForClass(RuleSet<Dataset, SimpleFD> projectedRuleset, Set<String> repairClass) throws RepairException
    {                       
        //Generate priority for that class
        List<String> priority = priorityManager.createPriority(
            sr,
            projectedRuleset,
            repairClass);
                
        if(VERBOSITY >= 2)
        {
            LOGGER.log(Level.INFO, "Start repair for class: {0}", repairClass);
            LOGGER.log(Level.INFO, "Priority model: {0}", priority);
        }

        //Initialize DSF structure: all tuples are in singleton classes
        dsf.init();

        //Get FDs sorted: first FDs with no repair attributes in LHS, then in order of priority first.
        List<SimpleFD> toRepair = sortFDs(
            priority,
            repairClass,
            projectedRuleset //We pass only FDs with RHS in repair class
                .stream()
                .filter(fd -> repairClass.contains(fd.getRhsAttribute()))
                .collect(Collectors.toList())
        );
        
        //Keep track of FDs visited
        List<SimpleFD> visited = new ArrayList<>();
        
        //Repair FDs in order, one by one
        while(!toRepair.isEmpty())
        {
            //poll
            SimpleFD nextFD = toRepair.remove(0);
            
            if(VERBOSITY >= 3)
                LOGGER.log(Level.INFO, "Treating FD {0}", nextFD);
            
            //Execute fix
            int fixes = fix(nextFD);
            visited.add(nextFD);
            
            if(VERBOSITY >= 3)
                LOGGER.log(Level.INFO, "Conducted {0} repairs", fixes);
            
            if(fixes > 0)
            {
                //Revise
                Set<SimpleFD> toRevise = visited
                    .stream()
                    .filter(fd -> fd
                        .getLeftHandSide()
                        .contains(nextFD.getRhsAttribute()))
                    .filter(fd -> 
                            fd.getLeftHandSide().size() > 1
                        ||  !preservationMap.get(nextFD.getRhsAttribute())
                    )
                    .collect(Collectors.toSet());
                
                visited.removeAll(toRevise);
                
                for(SimpleFD rfd: toRevise)
                {
                    int revisionPriority = priority.indexOf(rfd.getRhsAttribute());

                    for(int i=0; i<toRepair.size(); i++)
                    {
                        if(revisionPriority >= priority.indexOf(toRepair.get(i).getRhsAttribute()))
                        {
                            toRepair.add(i, rfd);
                            break;
                        }
                    }
                    
                    //If added nowhere, add in the back
                    if(!toRepair.contains(rfd))
                    {
                        toRepair.add(toRepair.size(), rfd);
                    }
                }
            }
        }        
    }
    
    private int fix(SimpleFD fd)
    {
        //Create a new partition that accounts for the FD
        dsf.update(fd, sr);
        
        if(VERBOSITY >= 5)
        {
            LOGGER.log(Level.INFO, "Updated partition: ", dsf.build(fd.getRhsAttribute()));
        }
        
        int fixes = 0;
        
        //Iterate over the row partition classes
        for(Set<Long> oids: dsf.build(fd.getRhsAttribute()))
        {                            
            //Is there a violation?
            boolean fdViolation = oids
                .stream()
                .map(id -> sr //Each id is mapped to the CURRENT value for fd.rhs
                    .getIdMapping()
                    .get(id)
                    .getSecond()
                    .get(fd.getRhsAttribute())) 
                .distinct()
                .count() != 1;
                      
            boolean regexViolation = regexes.get(fd.getRhsAttribute()) == null
                ? false
                : !oids
                .stream()
                .map(id -> sr //Each id is mapped to the CURRENT value for fd.rhs
                    .getIdMapping()
                    .get(id)
                    .getSecond()
                    .get(fd.getRhsAttribute())) 
                .allMatch(v -> v.toString().matches(regexes.get(fd.getRhsAttribute())));
            
            if(fdViolation || regexViolation)
            { 
                fixes++;
                Cell<?> fixValue = repair(
                    fd.getRhsAttribute(),
                    sr,
                    oids
                );
                
                if(VERBOSITY >= 5)
                {
                    System.out.println("Resolved by: " + fd.getRhsAttribute() + "=" + fixValue.getValue());
                }

                //Update the repair values in the SwipeRepair structure
                for(Long id: oids)
                {
                    sr
                    .getIdMapping()
                    .get(id)
                    .getSecond()
                    .update(fd.getRhsAttribute(), fixValue);
                }
            }
        }
  
        return fixes;
    }

    private List<SimpleFD> sortFDs(List<String> priority, Set<String> repairClass, List<SimpleFD> relevantRules) throws RepairException
    {
        //Identify FDs with LHS outside the class:
        //these FDs have fixed left-hand sides and are repaired first
        List<SimpleFD> sorted = relevantRules
            .stream()
            .filter(fd -> SetOperations
                .intersect(repairClass, fd.getLeftHandSide())
                .isEmpty())
            .collect(Collectors.toList());
        
        if(VERBOSITY >= 2)
            LOGGER.log(
                Level.INFO,
                "FDs with no repair attributes in LHS: {0}",
                sorted);
        
        //Next, we add FDs according the reliability of their RHS
        for(String rhs: priority)
        {
            for(SimpleFD fd: relevantRules)
            {
                if(fd.getRhsAttribute().equals(rhs)
                && !SetOperations.intersect(repairClass, fd.getLeftHandSide()).isEmpty())
                {
                    sorted.add(fd);
                }
            }
        }
        
        if(sorted.size() != relevantRules.size())
        {
            throw new RepairException("Sorted FDs do not contain all relevant FDs.");
        }
        
        return sorted;
    }
    
    private Cell repair(String a, SwipeRepair swipeRepair, Set<Long> ids)
    {
        //IDs cannot be empty
        if(ids.isEmpty())
            throw new RuntimeException("Unexpected empty set of ids during "
                + "Swipe repair for attribute '" 
                + a + "'.");
        
        //Collect relevant objects
        List<CellMap> cellsMaps = new ArrayList<>();
        
        long minCellNum = Long.MAX_VALUE;
        
        for(Long id: ids)
        {            
            //Get cell map for this id
            CellMap cm = fetch(swipeRepair
                .getIdMapping()
                .get(id));

            //Update min cell number
            minCellNum = Long.min(
                minCellNum,
                cm.get(a).getCellNumber()
            );
            
            cellsMaps.add(cm);
        }

        //Consolidate
        Object cValue = consolidate(cellsMaps,a);
        
        //Return consolidated cell, with consolidated value and updated cell number
        return new Cell<>(cValue, minCellNum);
    }

    private Object consolidate(List<CellMap> cellMaps, String a)
    {
        Set<Object> values = cellMaps
            .stream()
            .filter(cm -> cm.get(a).getValue() != null) //Now, we must check for null values here
            .map(cm -> (Object)cm.get(a).getValue())
            .distinct()
            .collect(Collectors.toSet());
        
        if(values.isEmpty())
            return null;
        
        Map<Object, Integer> candidateCostMap = regexes.get(a) != null
            ? values 
                .stream()
                .map(v -> v.toString())
                .flatMap(s -> Stream.concat(
                    (s.matches(regexes.get(a))
                        ? Stream.of(s)
                        : Stream.empty()),
                    (explainerMap.containsKey(a)
                        ? explainerMap.get(a).get(s).stream()
                        : Stream.empty()
                    )
                ))
                .distinct()
                .collect(Collectors.toMap(s->s, s->0))
            : values.stream().collect(Collectors.toMap(s->s, s->0));
        
        for(int i=0; i<cellMaps.size();i++)
        {
            DataObject context = cellMaps.get(i).toDataObject();
            
            //The candidate
            Object orig = cellMaps.get(i).get(a).getValue();
            
            for(Object cand: candidateCostMap.keySet())
            {
                candidateCostMap.merge(
                    cand,
                    costModel
                        .getCostFunction(a)
                        .cost(
                            orig,
                            cand,
                            context),
                    Integer::sum);
            }
        }
        
        return candidateCostMap
            .entrySet()
            .stream()
            .sorted(Comparator.comparing(Map.Entry<Object,Integer>::getValue))
            .map(e -> e.getKey())
            .findFirst()
            .orElse(null);
    }
    
    private CellMap fetch(Couple<CellMap> couple)
    {
        return propagate
            ? couple.getSecond()
            : couple.getFirst();
    }

    private void repairRegexOnly(Set<String> repairClass) throws RepairException
    {
        if(repairClass.isEmpty())
            return;
        
        if(repairClass.size() > 1)
            throw new RepairException(
                "Error: repair of regex only should happen only for singleton classes. "
            +   "Repair class was: " + repairClass);
        
        //Get the repair attribute
        String a = repairClass
            .stream()
            .findFirst()
            .get();
        
        if(regexes.get(a) == null)
            return;
        
        //Get the regex
        String regex = regexes.get(a);
        
        Set<String> validValues = sr
            .getIdMapping()
            .values()
            .stream()
            .map(c -> c.getFirst().get(a).getValue())
            .filter(v -> v != null && v instanceof String)
            .filter(v -> v.toString().matches(regex))
            .map(Object::toString)
            .collect(Collectors.toSet());
        
        CostFunction cf = costModel.getCostFunction(a);
   
        if(cf == null)
            return;

        for(Long oid: sr.getIdMapping().keySet())
        {
            Couple<CellMap> couple = sr.getIdMapping().get(oid);
            
            Object value = couple.getFirst().get(a).getValue();
            
            if(value == null || !(value instanceof String))
                continue;
            
            if(value.toString().matches(regex))
                continue;
            
            DataObject ctx = couple.getFirst().toDataObject();
            
            Set<String> expl = explainerMap.containsKey(a)
                && explainerMap.get(a).containsKey(value.toString())
                ? explainerMap.get(a).get(value.toString())
                : new HashSet<>();
            
            Pair<String,Integer> fix = new Pair<>(null, Integer.MAX_VALUE);
            
            //Check for explanations
            if(!expl.isEmpty())
            {
                for(String e: expl)
                {
                    int cost = cf.cost(value.toString(), e, ctx);
                    
                    if(cost < fix.getSecond())
                    {
                        fix = new Pair<>(e,cost);
                    }
                }
            }
            //Else, check valid values
            else if(!validValues.isEmpty())
            {
                for(String v: validValues)
                {
                    int cost = cf.cost(value.toString(), v, ctx);
                    
                    if(cost < fix.getSecond())
                    {
                        fix = new Pair<>(v,cost);
                    }
                }
            }
                     
            //Update the repair values in the SwipeRepair structure
            sr
            .getIdMapping()
            .get(oid)
            .getSecond()
            .update(
                a,
                new Cell<>(
                    fix.getFirst(),
                    couple.getFirst().get(a).getCellNumber())
            );
        }                              
    }

    /**
     * During pre-cleaning, we detect suspicious values in attributes that are never
     * in the RHS of any FD
     * @param partition 
     */
    private void preClean(String attr)
    {        
        for(SimpleFD fd: ruleset)
        {
            //We consider only FDs where at least one of attrs is present
            if(!fd.getLeftHandSide().contains(attr))
                continue;
            
            Map<DataObject, Multiset<Object>> distributions = new HashMap<>();
            
            for(Long id: sr.getIdMapping().keySet())
            {
                //Get determinant
                DataObject determinant = sr
                    .getIdMapping()
                    .get(id)
                    .getSecond()
                    .project(fd.getLeftHandSide())
                    .toDataObject();
                
                if(fd.getLeftHandSide().stream().anyMatch(a -> determinant.get(a) == null))
                    continue;

                if(!distributions.containsKey(determinant))
                {
                    distributions.put(determinant, new Multiset<>());
                }
                
                Object rhsValue = sr
                    .getIdMapping()
                    .get(id)
                    .getSecond()
                    .get(fd.getRhsAttribute())
                    .getValue();
                
                //If RHS value is null, we don't add it
                if(rhsValue == null)
                    continue;
                
                String regex = regexes.get(fd.getRhsAttribute());
                
                //If RHS value is invalied, we don't add it
                if(regex != null && !rhsValue.toString().matches(regex))
                    continue;
                
                //Add value
                distributions
                    .get(determinant)
                    .add(rhsValue);
            }
            
            Set<DataObject> anomalies = distributions
                .entrySet()
                .stream()
                .filter(e -> entropy(e.getValue()) > this.rhsEntropyBound)
                .map(e -> e.getKey())
                .collect(Collectors.toSet());
            
            if(anomalies.isEmpty())
                continue;
            
            if(VERBOSITY >= 3)
            {
                LOGGER.log(Level.INFO, "Anomalous values found:");
                for(DataObject o: anomalies)
                {
                    LOGGER.log(Level.INFO, o.toString());
                }
            }
            
            //Clean phase
            for(Long id: sr.getIdMapping().keySet())
            {
                //Get determinant
                DataObject determinant = sr
                    .getIdMapping()
                    .get(id)
                    .getSecond()
                    .project(fd.getLeftHandSide())
                    .toDataObject();
                
                if(anomalies.contains(determinant))
                {
                    long cellNumber = sr
                        .getIdMapping()
                        .get(id)
                        .getSecond()
                        .get(attr)
                        .getCellNumber();
                    
                    sr
                        .getIdMapping()
                        .get(id)
                        .getSecond()
                        .update(
                            attr,
                            new Cell<>(null,cellNumber)
                        );
                }
            }   
        }
    }
    
    private double entropy(Multiset<Object> bag)
    {
        int card = bag.cardinality();
        
        return -1.0 * bag
            .keySet()
            .stream()
            .mapToDouble(o -> UnitScore.fromFraction(bag.multiplicity(o), card).getValue())
            .map(pr -> pr * Math.log(pr)/Math.log(2.0))
            .sum();
    }
}
