package be.ugent.ledc.fundy.algorithms.discovery.tane;

import be.ugent.ledc.fundy.datastructures.StrippedPartition;
import be.ugent.ledc.fundy.datastructures.PrefixBlock;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import be.ugent.ledc.fundy.util.LexicographicComparator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * An implementation of FDMiner that uses the TANE algorithm to find all minimal and non-trivial
 * FDs that are valid in a given dataset.
 * @author abronsel
 */
public class TaneMiner implements FDMiner
{
    private final Set<String> tableSchema;
    
    private final double errorThreshold;
    
    private final int maxLevel;

    public TaneMiner(Set<String> tableSchema, double errorThreshold, int maxLevel)
    {
        this.tableSchema = tableSchema;
        this.errorThreshold = errorThreshold;
        this.maxLevel = maxLevel;
    }
    
    public TaneMiner(Set<String> tableSchema, double errorThreshold)
    {
        this(tableSchema, errorThreshold, Integer.MAX_VALUE);
    }

    public TaneMiner(Set<String> tableSchema)
    {
        this(tableSchema, 0.0);
    }

    /**
     * Finds all minimal and non-trivial FDs that are satisfied in the given dataset
     * by using the TANE algorithm. This is a lattice traversal algorithm that uses
     * row partitions to test FDs and uses several pruning strategies that arise from
     * the definition of minimal FDs.
     * @param dataset
     * @return
     * @throws TaneException 
     */
    @Override
    public List<SimpleFD> discover(Dataset dataset) throws TaneException
    {
        //Initialize a list of minimal FDs
        List<SimpleFD> minimalFunctionalDependencies = new ArrayList<>();
        
        //Generate singleton partitions
        Map<Set<String>, StrippedPartition> partitions = StrippedPartitionGenerator
                .generateSingletonPartitions(tableSchema, dataset);
        
        Map<Set<String>, Set<String>> rhsCandidates = new HashMap<>();
        
        //Initially, each attribute is an RHS candidate for each set
        for(String attribute: tableSchema)
        {
            rhsCandidates.put(
                Stream
                    .of(attribute)
                    .collect(Collectors.toSet()),
                new HashSet<>(tableSchema)
            );
        }
        
        boolean stop = false;
        
        int l=1;
        List<Set<String>> previousLevel = new ArrayList<>();
        
        while(!stop)
        {
            System.out.println("Current level index = " + l);
            
            List<Set<String>> currentLevel = next(l, previousLevel);
            
            if(currentLevel.isEmpty())
            {
                System.out.println("Level is empty... Stopping!");
                stop = true;
            }
            else if(l > maxLevel)
            {
                System.out.println("Maximal level achied... Stopping!");
                stop = true;
            }
            else if(l > 1)
            {
                System.out.println("Computing dependencies for level " + l);
                minimalFunctionalDependencies.addAll(computeDependencies(
                    currentLevel,
                    rhsCandidates,
                    partitions,
                    dataset.getSize())
                );
                
                System.out.println("Pruning for level " + l);
                prune(currentLevel, rhsCandidates, partitions, minimalFunctionalDependencies);
            }
            l++;
            
            
//            System.out.println("RHS candidates at the end of level " + l);
//            for(Set<String> attributes: rhsCandidates.keySet())
//            {
//                System.out.println(attributes + " -> " + rhsCandidates.get(attributes).stream().collect(Collectors.joining(", ")));
//            }
            
            previousLevel = currentLevel;
        }
        
        return minimalFunctionalDependencies;
        
    }

    private List<SimpleFD> computeDependencies(List<Set<String>> currentLevel, Map<Set<String>, Set<String>> rhsCandidates, Map<Set<String>, StrippedPartition> partitions, int rows) throws TaneException
    {
        List<SimpleFD> minimalDependencies = new ArrayList<>();
        
        Map<Set<String>, Set<String>>   newRhsCandidates    = new HashMap<>();
        Map<Set<String>, StrippedPartition>     newPartitions       = new HashMap<>();
        
        for(Set<String> candidate: currentLevel)
        {
            //Generate the possible RHSs for the candidate
            Set<String> rhsc = null;
            
            for(String attribute: candidate)
            {
                //Calculate the subset of the candidate without the attribute
                Set<String> subset = new HashSet<>(candidate);
                subset.remove(attribute);
                
                //Get rhses
                Set<String> subsetRhs = rhsCandidates.get(subset);
                
                if(rhsc == null)
                {
                    rhsc = new HashSet<>(subsetRhs);
                }
                else
                {
                    rhsc.retainAll(subsetRhs);
                }
            }
            
            if(rhsc != null && !rhsc.isEmpty())
            {
                newRhsCandidates.put(candidate, rhsc);
            }
            
            Set<String> toCheck = new HashSet<>(candidate);
            toCheck.retainAll(rhsc);
            
            if(!toCheck.isEmpty())
            {
                //Find partitions of subsets with size - 1
                List<StrippedPartition> subsetPartitions = partitions
                    .entrySet()
                    .stream()
                    .filter(e -> candidate.containsAll(e.getKey()) && candidate.size() == e.getKey().size() + 1)
                    .map(e -> e.getValue())
                    .collect(Collectors.toList());
                
                if(subsetPartitions.size() < 2)
                    throw new TaneException("Cannot calculate partition for X = " + candidate);
                
                StrippedPartition candidatePartition = subsetPartitions
                    .get(0)
                    .product(subsetPartitions.get(1));
                newPartitions.put(candidate, candidatePartition);
                
                for(String rhsAttribute: toCheck)
                {
                    Set<String> lhs = new HashSet<>(candidate);
                    lhs.remove(rhsAttribute);
                    
                    if(partitions.get(lhs) == null)
                        throw new TaneException("Unknown partition for LHS " + lhs);

                    //Test dependency
                    if(test(candidatePartition, partitions.get(lhs), rows))
                    {
                        SimpleFD sfd = new SimpleFD(lhs, rhsAttribute);
                        
                        minimalDependencies.add(sfd);
                        
                        System.out.println("Adding FD: " + sfd);
                        
                        //Clean candidate RHS attributes
                        newRhsCandidates
                            .get(candidate)
                            .remove(rhsAttribute);
                        newRhsCandidates
                            .get(candidate)
                            .removeAll(tableSchema
                                .stream()
                                .filter(a -> !candidate.contains(a))
                                .collect(Collectors.toSet()));
                    }
                }
            }
        }
        
        //Pass new data
        partitions.clear();
        partitions.putAll(newPartitions);
        
        rhsCandidates.clear();
        rhsCandidates.putAll(newRhsCandidates);
        
        return minimalDependencies;
    }
    
    private boolean test(StrippedPartition candidatePartition, StrippedPartition lhsPartition, int rows)
    {
        //If the error threshold, we want dependencies that are satisfied
        if(Math.abs(this.errorThreshold) <= Double.MIN_VALUE)
            return candidatePartition.getError() == lhsPartition.getError();
        
        //Else: we search for approximate dependencies
        
        //Bounding rules: for X->a we know e(X) - e(Xa) <= e(X->a) <= e(X)
        double upperBound = lhsPartition.getError() / (double)rows;
        double lowerBound = upperBound - (candidatePartition.getError() / (double)rows);

        //Bounding rule 1
        if(upperBound <= this.errorThreshold)
            return true;
        
        //Bounding rule 2
        if(lowerBound > this.errorThreshold)
            return false;
        
        //If bounding rules do not allow a decision: compute e(X->a) exactly
        return candidatePartition.exactError(lhsPartition) <= errorThreshold;
    }
    
    private void prune(List<Set<String>> currentLevel, Map<Set<String>, Set<String>> rhsCandidates, Map<Set<String>, StrippedPartition> partitions, List<SimpleFD> dependencies)
    {
        for(int i=0; i<currentLevel.size(); i++)
        {
            Set<String> levelCandidate = currentLevel.get(i);
            
            if(rhsCandidates.get(levelCandidate) == null || rhsCandidates.get(levelCandidate).isEmpty())
            {
                currentLevel.remove(i);
                i--;
            }
            else if(partitions.get(levelCandidate).isKey())
            {
                System.out.println("Found key during pruning step: " + levelCandidate);
                
                Set<String> toCheck = new HashSet<>(rhsCandidates.get(levelCandidate));
                toCheck.removeAll(levelCandidate);
                                
                for(String rhsAttribute: toCheck)
                {
//                    boolean notYetGenerated = levelCandidate.stream().allMatch(b ->
//                    {
//                        HashSet<String> copy = new HashSet<>(levelCandidate);
//                        copy.add(rhsAttribute);
//                        copy.remove(b);
//                        return rhsCandidates.get(copy).contains(rhsAttribute);
//                    });
                    
                    boolean notYetGenerated = true;
                    
                    for(String b: levelCandidate)
                    {
                        HashSet<String> copy = new HashSet<>(levelCandidate);
                        copy.add(rhsAttribute);
                        copy.remove(b);
                        if(rhsCandidates.get(copy) == null || !rhsCandidates.get(copy).contains(rhsAttribute))
                        {
                            notYetGenerated = false;
                            break;
                        }
                    }
                    
                    if(notYetGenerated)
                    {
                        SimpleFD fd = new SimpleFD(levelCandidate, rhsAttribute);
                        dependencies.add(fd);
                        System.out.println("Adding FD: " + fd);
                    }
                }
                currentLevel.remove(i);
                i--;
            }
        }
    }
    
    private List<Set<String>> next(int l, List<Set<String>> previousLevel)
    {
        //For level 1, return all attributes wrapped in sets
        if(l == 1)
        {
            return tableSchema
                .stream()
                .map(a -> Stream
                    .of(a)
                    .collect(Collectors.toSet()))
                .collect(Collectors.toList());
        }
        else
        {
            //First sort the candidates
            List<List<String>> sortedCandidates = previousLevel
                .stream()
                .map(set -> set
                    .stream()
                    .sorted()
                    .collect(Collectors.toList()))
                .collect(Collectors.toList());
            
            //Now sort the list
            Collections.sort(sortedCandidates, new LexicographicComparator());
            
            List<PrefixBlock> prefixBlocks = new ArrayList<>();
            
            for(List<String> sortedCandidate: sortedCandidates)
            {
                if(prefixBlocks.isEmpty())
                {
                    prefixBlocks
                        .add(new PrefixBlock(Stream
                            .of(sortedCandidate)
                            .collect(Collectors.toList())));
                }
                else if(prefixBlocks.get(prefixBlocks.size() - 1).getLast().subList(0, l-2).equals(sortedCandidate.subList(0, l-2)))
                {
                    //Add to last prefixblock
                    prefixBlocks
                        .get(prefixBlocks.size() - 1)
                        .add(sortedCandidate);
                }
                else
                {
                    prefixBlocks.add(new PrefixBlock(Stream
                        .of(sortedCandidate)
                        .collect(Collectors.toList())));
                }
            }
            
//            System.out.println("Previous level " + previousLevel);
//            System.out.println("Prefix blocks for level " + l);
            
//            for(PrefixBlock block: prefixBlocks)
//            {
//                System.out.println(block.getItems());
//            }
            
            List<Set<String>> currentLevel = new ArrayList<>();
            
            for(PrefixBlock prefixBlock: prefixBlocks)
            {
                for(int i=0; i<prefixBlock.getItems().size(); i++)
                {
                    for(int j=i+1; j<prefixBlock.getItems().size(); j++)
                    {
                        Set<String> levelCandidate = new HashSet<>(prefixBlock.getItems().get(i));
                        levelCandidate.addAll(prefixBlock.getItems().get(j));
                        
                        boolean addCandidate = true;
                        
                        for(String attribute: levelCandidate)
                        {
                            Set<String> subset = new HashSet<>(levelCandidate);
                            subset.remove(attribute);
                            
                            if(!previousLevel.contains(subset))
                            {
                                addCandidate = false;
                                break;
                            }
                        }
                        
                        if(addCandidate)
                        {
                            currentLevel.add(levelCandidate);
                        }
                    }
                }
            }
            
            return currentLevel;
        }
    }
}
