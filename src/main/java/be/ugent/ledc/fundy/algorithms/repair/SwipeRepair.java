package be.ugent.ledc.fundy.algorithms.repair;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.Couple;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class SwipeRepair
{
    /**
     * A mapping from identifiers to a couple of objects: the first object
     * holds the original, the second the repair
     */
    private final Map<Long, Couple<CellMap>> idMapping;
    
    public static final String LEDC_KEY = "#ledc_key#";
    
    public SwipeRepair(Dataset dataset, Set<String> fdAttributes)
    {
        idMapping = new HashMap<>();
        
        long id=0;
        
        //Make a map
        for(DataObject o: dataset)
        {
            //Values are wrapped into cells, such that null values becomes numbered variables.
            //We attach an id to a couple: the first is the original object, the second the repair
            idMapping.put(
                id,
                new Couple<>(
                    toCellMap(new DataObject(o).set(LEDC_KEY, id), id, fdAttributes),
                    toCellMap(new DataObject(o).set(LEDC_KEY, id), id, fdAttributes)
                ));
            
            id++;
        }
    }

    public Map<Long, Couple<CellMap>> getIdMapping()
    {
        return idMapping;
    }

    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.idMapping);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final SwipeRepair other = (SwipeRepair) obj;
        return Objects.equals(this.idMapping, other.idMapping);
    }
    
    public List<DataObject> getRepair()
    {
        return idMapping
            .entrySet()
            .stream()
            .sorted(Comparator.comparing(Map.Entry::getKey))//Sort by id
            .map(e -> e.getValue().getSecond().toDataObject())
            .collect(Collectors.toList());
    }
    
    private CellMap toCellMap(DataObject o, long cellNumber, Set<String> fdAttributes)
    {
        CellMap cm = new CellMap(o
            .getAttributes()
            .stream()
            .collect(Collectors.toMap(
                a -> a,
                a -> new Cell(o.get(a), cellNumber))
            )
        );
        
        fdAttributes
            .stream()
            .filter(a -> cm.get(a) == null)
            .forEach(a -> cm.update(a, new Cell<>(null, cellNumber)));
        
        return cm;
    }
}
