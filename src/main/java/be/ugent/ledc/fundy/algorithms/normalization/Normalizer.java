package be.ugent.ledc.fundy.algorithms.normalization;

import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.NormalisationException;
import java.util.Map;

public interface Normalizer
{
    public Map<TableSchema, RuleSet<Dataset, FD>> decompose(TableSchema schema, RuleSet<Dataset, FD> ruleset) throws NormalisationException;
}
