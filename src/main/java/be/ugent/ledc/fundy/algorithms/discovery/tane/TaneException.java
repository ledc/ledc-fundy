package be.ugent.ledc.fundy.algorithms.discovery.tane;

public class TaneException extends DependencyMiningException
{

    public TaneException(String message)
    {
        super(message);
    }

    public TaneException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public TaneException(Throwable cause)
    {
        super(cause);
    }

    public TaneException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}
