package be.ugent.ledc.fundy.algorithms.repair;

import be.ugent.ledc.core.dataset.DataObject;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class CellMap
{
    private final Map<String,Cell<?>> map;

    public CellMap(Map<String, Cell<?>> map)
    {
        this.map = map;
    }

    public Map<String, Cell<?>> getMap()
    {
        return map;
    }
    
    public Cell<?> get(String a)
    {
        return this.map.get(a);
    }

    public void update(String a, Cell<?> cell)
    {
        this.map.put(a, cell);
    }
    
    @Override
    public int hashCode()
    {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.map);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final CellMap other = (CellMap) obj;
        return Objects.equals(this.map, other.map);
    }
    
    public CellMap project(Set<String> attributes)
    {
        return new CellMap(this.map
        .entrySet()
        .stream()
        .filter(e -> attributes.contains(e.getKey()))
        .collect(Collectors.toMap(
            e -> e.getKey(),
            e -> e.getValue())
        ));
    }

    public DataObject toDataObject()
    {
        DataObject o = new DataObject();
        
        this
            .getMap()
            .keySet()
            .stream()
            .forEach(a -> o.set(a, get(a).getValue()));
        
        return o;
    }
    
    @Override
    public String toString()
    {
        return map.toString();
    }
}
