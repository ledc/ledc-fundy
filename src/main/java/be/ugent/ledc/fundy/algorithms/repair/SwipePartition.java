package be.ugent.ledc.fundy.algorithms.repair;

import be.ugent.ledc.fundy.datastructures.SimpleFD;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Models a partition of the attributes in such a way that it is possible
 * to repair the data for attributes of each partition class in one go
 * by changing only attributes of the class but satisfying all FDs involving attributes of this
 * and previous classes.
 */
public class SwipePartition implements Iterable<Set<String>>
{
    private final List<Set<String>> repairClasses;

    public SwipePartition()
    {
        repairClasses = new ArrayList<>();
    }

    public void addRepairClass(Set<String> repairClass)
    {
        this.repairClasses.add(repairClass);
    }
    
    public void pushRepairClass(Set<String> repairClass)
    {
        this.repairClasses.add(0, repairClass);
    }

    @Override
    public Iterator<Set<String>> iterator()
    {
        return repairClasses.iterator();
    }
    
    public Stream<Set<String>> stream()
    {
        return repairClasses.stream();
    }
    
    public int classIndexOf(String a)
    {
        for(int i=0; i < repairClasses.size(); i++)
            if(repairClasses.get(i).contains(a))
                return i;
        
        return -1;
    }
    
    public boolean forward(SimpleFD fd)
    {
        int rhsIndex = repairClasses
            .indexOf(repairClasses
                .stream()
                .filter(c -> c.contains(fd.getRhsAttribute()))
                .findFirst()
                .get()
            );
        
        return repairClasses
            .subList(rhsIndex + 1, repairClasses.size())
            .stream()
            .allMatch(c -> fd
                .getLeftHandSide()
                .stream()
                .noneMatch(a -> c.contains(a))
            );
    }

    @Override
    public String toString()
    {
        return repairClasses
            .stream()
            .map(c -> c.stream().collect(Collectors.joining(",","[","]")))
            .collect(Collectors.joining());
    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 11 * hash + Objects.hashCode(this.repairClasses);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final SwipePartition other = (SwipePartition) obj;
        if (!Objects.equals(this.repairClasses, other.repairClasses))
        {
            return false;
        }
        return true;
    }   
}
