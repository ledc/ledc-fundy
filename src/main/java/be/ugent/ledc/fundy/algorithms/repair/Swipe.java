package be.ugent.ledc.fundy.algorithms.repair;

import be.ugent.ledc.core.cost.CostModel;
import be.ugent.ledc.fundy.algorithms.repair.priority.PriorityManager;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.algorithms.repair.priority.ReliabilityPriority;
import be.ugent.ledc.fundy.datastructures.FD;
import java.util.HashMap;

/**
 * Swipe is an algorithm for repairing violations of FDs in a table. It can be
 * thought of as a simplified version of the Chase algorithm where some assumptions are
 * made in order to make repairing significantly faster, at the cost of potentially
 * not finding the most optimal repair in terms of attribute changes.
 * 
 * The first key idea is to partition the attributes involved in FDs and rank classes
 * of that partition such that for each class, we can forwardly repair attributes
 * in that class without violating FDs involving earlier repaired attributes.
 * Forward repairing means change the RHS of the FD.
 * 
 * In each repair step, we change attributes from one partition class and do this
 * until all classes have been visited. In the end, we obtain a clean dataset
 * that satisfies all FDs.
 * 
 * The second key idea is that in order to manage FD violations, we make use
 * of disjoint set forests to manage partitions along the way.
 * @author abronsel
 */
public class Swipe extends SwipeR
{
    public Swipe(RuleSet<Dataset, FD> ruleset, CostModel costModel, PriorityManager priorityManager, boolean propagate)
    {
        super(ruleset, costModel, priorityManager, new HashMap<>(), propagate, 1000.0);
    }
    
    public Swipe(RuleSet<Dataset, FD> ruleset, CostModel costModel, PriorityManager priorityManager)
    {
        super(ruleset, costModel, priorityManager);
    }
    
    public Swipe(RuleSet<Dataset, FD> ruleset, CostModel costModel, boolean propagate)
    {
        this(ruleset, costModel, new ReliabilityPriority(), propagate);
    }
    
    public Swipe(RuleSet<Dataset, FD> ruleset, CostModel costModel)
    {
        this(ruleset, costModel, new ReliabilityPriority(), true);
    }
}
