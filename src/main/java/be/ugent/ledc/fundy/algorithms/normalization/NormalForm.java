package be.ugent.ledc.fundy.algorithms.normalization;

public enum NormalForm
{
    _1NF(1),
    _2NF(2),
    _3NF(3),
    BCNF(4);
    
    private final int level;
    
    private NormalForm(int level)
    {
        this.level = level;
    }

    public int getLevel()
    {
        return level;
    }
}
