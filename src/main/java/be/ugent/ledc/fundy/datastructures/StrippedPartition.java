package be.ugent.ledc.fundy.datastructures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class StrippedPartition implements Iterable<Set<Integer>>
{
    private final List<Set<Integer>> nonSingletonClasses;
    private final int e;
    private final int rows;

    public StrippedPartition(List<Set<Integer>> classes, int rows)
    {
        this.nonSingletonClasses = classes
            .stream()
            .filter(set -> set.size() > 1)
            .collect(Collectors.toList());
        this.e = this.nonSingletonClasses
            .stream()
            .mapToInt(set -> set.size())
            .sum()
            - this.nonSingletonClasses.size();
        this.rows = rows;
    }

    public List<Set<Integer>> getNonSingletonClasses()
    {
        return nonSingletonClasses;
    }

    public int getError()
    {
        return e;
    }
    
    public boolean equivalent(Integer i1, Integer i2)
    {
        return nonSingletonClasses
            .stream()
            .anyMatch(c -> c.contains(i1) && c.contains(i2));
    }
    
    public boolean equivalent(Set<Integer> indices)
    {
        return nonSingletonClasses
            .stream()
            .anyMatch(c -> c.containsAll(indices));
    }
    
    public void makeEquivalent(Set<Integer> indices)
    {
        Set<Integer> newClass = this.nonSingletonClasses
            .stream()
            .filter(c -> indices.stream().anyMatch(i -> c.contains(i)))
            .flatMap(c -> c.stream())
            .collect(Collectors.toSet());
        
        this.nonSingletonClasses.removeIf(c -> indices.stream().anyMatch(i -> c.contains(i)));
        this.nonSingletonClasses.add(newClass);
    }

    public boolean isKey()
    {
        return nonSingletonClasses.isEmpty();
    }

    public StrippedPartition product(StrippedPartition p)
    {
        int[] lookup = new int[rows];
        Arrays.fill(lookup, -1);

        List<Set<Integer>> productNonSingletonClasses = new ArrayList<>();

        List<Set<Integer>> tempPartition = new ArrayList<>();

        //Iterate over first partition
        for (int i = 0; i < nonSingletonClasses.size(); i++)
        {
            for (Integer rowId : nonSingletonClasses.get(i))
            {
                lookup[rowId] = i;
            }

            tempPartition.add(new HashSet<>());
        }

        //Iterate over second partition
        for (int i = 0; i < p.getNonSingletonClasses().size(); i++)
        {
            for (Integer rowId : p.getNonSingletonClasses().get(i))
            {
                if (lookup[rowId] != -1)
                {
                    tempPartition.get(lookup[rowId]).add(rowId);
                }
            }

            for (Integer rowId : p.getNonSingletonClasses().get(i))
            {
                if (lookup[rowId] != -1)
                {
                    if (tempPartition.get(lookup[rowId]).size() > 1)
                    {
                        productNonSingletonClasses.add(new HashSet<>(tempPartition.get(lookup[rowId])));
                    }
                    tempPartition.set(lookup[rowId], new HashSet<>());
                }
            }
        }

        return new StrippedPartition(productNonSingletonClasses, rows);
    }

    /**
     * Produces the error of FD X -> a IF AND ONLY IF the current partition is
     * a partition for Xa and the given partition is a partition for X
     * @param lhsPartition The partition for the LHS of the FD for which the error must be computed.
     * @return The error for the FD X -> a
     */
    public double exactError(StrippedPartition lhsPartition)
    {
        int[] lookup = new int[rows];
        Arrays.fill(lookup, 0);

        int error = 0;
        
        for(Set<Integer> nsClass: nonSingletonClasses)
        {
            lookup[nsClass.stream().mapToInt(i->i).min().getAsInt()] = nsClass.size();
        }
        
        for(Set<Integer> lhsClass: lhsPartition.getNonSingletonClasses())
        {
            int m = 1;
            
            for(Integer i: lhsClass)
            {
                m = Math.max(m, lookup[i]);
            }
            
            error += lhsClass.size() - m;
        }
        
        return error/(double)rows;

    }
    
    public int size()
    {
        return nonSingletonClasses.size();
    }

    @Override
    public String toString()
    {
        return nonSingletonClasses.toString();
    }

    @Override
    public Iterator<Set<Integer>> iterator()
    {
        return nonSingletonClasses.iterator();
    }
}

