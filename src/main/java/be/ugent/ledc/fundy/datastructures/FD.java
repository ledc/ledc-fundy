package be.ugent.ledc.fundy.datastructures;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.Rule;
import be.ugent.ledc.fundy.io.ConstraintIo;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * A class that represents a functional dependency.
 * @author abronsel
 */
public class FD implements Rule<Dataset>
{
    private final Set<String> leftHandSide;
    
    private final Set<String> rightHandSide;

    public FD(Set<String> leftHandSide, Set<String> rightHandSide)
    {
        this.leftHandSide = leftHandSide;
        this.rightHandSide = rightHandSide;
    }

    public Set<String> getLeftHandSide()
    {
        return leftHandSide;
    }

    public Set<String> getRightHandSide()
    {
        return rightHandSide;
    }
    
    @Override
    public Set<String> getInvolvedAttributes()
    {
        return Stream.concat(
            leftHandSide.stream(),
            rightHandSide.stream())
        .collect(Collectors.toSet());
    }
    
    /**
     * True if a is in the right hand side of this FD
     * @param a
     * @return 
     */
    public boolean implies(String a)
    {
        return this.rightHandSide.contains(a);
    }

    @Override
    public int hashCode()
    {
        int hash = 3;
        hash = 59 * hash + Objects.hashCode(this.leftHandSide);
        hash = 59 * hash + Objects.hashCode(this.rightHandSide);
        return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        final FD other = (FD) obj;
        if (!Objects.equals(this.leftHandSide, other.leftHandSide))
        {
            return false;
        }
        if (!Objects.equals(this.rightHandSide, other.rightHandSide))
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return leftHandSide
            .stream()
            .collect(Collectors.joining(ConstraintIo.SEPARATOR))
            .concat(ConstraintIo.IMPLICATION)
            .concat(rightHandSide
                .stream()
                .collect(Collectors.joining(ConstraintIo.SEPARATOR))
            );
    }

    @Override
    public boolean test(Dataset dataset)
    {
        Map<DataObject, List<DataObject>> grouped = dataset
            .stream()
            .filter(o -> leftHandSide.stream().allMatch(a -> o.get(a) != null))
            .collect(Collectors.groupingBy(o -> o.project(leftHandSide)));
        
        //dataset satisfies the FD if all groups have the same value for RHS attributes.
        return grouped
            .values()
            .stream()
            .allMatch(objs -> objs
                .stream()
                .map(o -> o.project(rightHandSide))
                .distinct()
                .count() == 1l
            );
    }
}
