package be.ugent.ledc.fundy.datastructures;

import java.util.List;

public class PrefixBlock
{

    private final List<List<String>> items;

    public PrefixBlock(List<List<String>> items)
    {
        this.items = items;
    }

    public List<List<String>> getItems()
    {
        return items;
    }

    public void add(List<String> item)
    {
        items.add(item);
    }

    public List<String> getLast()
    {
        return items.isEmpty() ? null : items.get(items.size() - 1);
    }
}
