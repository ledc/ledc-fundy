package be.ugent.ledc.fundy.datastructures;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toSet;

/**
 * A Disjoint Set Forest is a data structure to represent partitions of an objects space S.
 * That is, it represents a set of disjoint (non-overlapping) subsets of S. 
 * A particular property of this data structure is that is can efficiently 
 compute the merge of two partitions.
 * @author abronsel
 */
public class DisjointSetForest<O>
{
    /**
     * A mapping that assigns each object to a parent.
     */
    private final Map<O, O> parents;
    
    /**
     * A mapping that keeps the sizes of the sub trees.
     */
    private final Map<O, Integer> sizes;
    
    private int numberOfPartitionClasses = 0;
    
    public DisjointSetForest()
    {
        this.parents = new LinkedHashMap<>();
        this.sizes = new HashMap<>();
    }

    /**
     * Returns true if this object is already in the forest
     * @param o
     * @return 
     */
    public boolean contains(O o)
    {
        return parents.get(o) != null;
    }

    public Map<O, O> getParents()
    {
        return parents;
    }

    public Map<O, Integer> getSizes()
    {
        return sizes;
    }
    
    /**
     * Finds the representative object for the set in which the object resides.
     * This implementation is based on the path halving algorithm by Tarjan
     * and Van Leeuwen [1]. See also:
     * 
     * https://en.wikipedia.org/wiki/Disjoint-set_data_structure#Finding_set_representatives
     * 
     * [1] Tarjan, Robert E.; van Leeuwen, Jan (1984).
     * "Worst-case analysis of set union algorithms".
     * Journal of the ACM. 31 (2): 245-281.
     * doi:10.1145/62.2160
     * 
     * @param o
     * @return 
     */
    public O find(O o)
    {
        if(!contains(o))
            throw new RuntimeException("Cannot find " + o + ". Cause: not present in partition.");
        
        O representative = o;
        while(!parents.get(representative).equals(representative))
        {
            parents.put(representative, parents.get(parents.get(representative)));
            representative = parents.get(representative);
        }
        
        return representative;
    }
    
    /**
     * Merges the sets in which two given objects o1 and o2 reside. In this implementation,
     * it is first verified if both objects are present. It this is not the case,
     * they are added first.
     * 
     * @param o1
     * @param o2
     */
    public void merge(O o1, O o2)
    {
        //Check if o1 is present and if not, add it.
        if(!contains(o1) || ! contains(o2))
            throw new RuntimeException("Attempt to merge identifiers "
                + o1
                + " and "
                + o2
                + " failed because one of them is not present.");
            
        //Get roots
        O x = find(o1);
        O y = find(o2);
        
        //Check if objects are already in the same set
        if(x.equals(y))
            return;

        int sizeX = sizes.get(x),sizeY=sizes.get(y);
        
        //The node with more descendants becomes the parent.
        //If the two nodes have the same number of descendants, then either one can become the parent.
        //In both cases, the size of the new parent node is set to its new total number of descendants.
        
        if (sizeX >= sizeY)
        {
            //x becomes the new root
            parents.put(y, x);
            sizes.put(x, sizeX + sizeY);
        }
        else
        {
            //y becomes the new root
            parents.put(x, y);
            sizes.put(y, sizeX + sizeY);
        }

        //Update number of partition classes
        numberOfPartitionClasses--;

    }
    
    public void add(O o)
    {
        parents.put(o, o);
        sizes.put(o, 1);
        numberOfPartitionClasses++;
    }

    /**
     * Returns true if two ids are in the same set and are thus equivalent
     * under the partition of the object space.
     * @param o1
     * @param o2
     * @return 
     */
    public boolean equivalent(O o1, O o2)
    {
        return find(o1).equals(find(o2));
    }
    
    /**
     * Get number of partition classes.
     * @return 
     */
    public int getNumberOfPartitionClasses()
    {
        return numberOfPartitionClasses;
    }
    
    /**
     * Builds a the actual partition classes by grouping objects that are equivalent.
     * @return 
     */
    public Collection<Set<O>> build()
    {
        return parents
            .keySet()
            .stream()
            .collect(Collectors.groupingBy(o -> find(o), toSet()))
            .values();
    }
}
