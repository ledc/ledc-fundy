package be.ugent.ledc.fundy.datastructures;

import be.ugent.ledc.core.dataset.DataObject;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class DecoratedForest
{
    /**
     * A mapping that assigns each object to a parent.
     */
    private final Map<Long, Long> parents;
    
    /**
     * A mapping that keeps the sizes of the sub trees.
     */
    private final Map<Long, Integer> sizes;
    
    /**
     * Keeps, for each determinant, the representative
     */
    private final Map<DataObject, Long> determinants;
    
    /**
     * Keeps, for each representative, the determinant
     */
    private final Map<Long, DataObject> invDeterminants;
    
    /**
     * The FD for which this forest represents equivalence classes
     */
    private final SimpleFD fd;
    
    /**
     * Keeps, for each node, a set of children for that node.
     */
    private final Map<Long, Set<Long>> children;
    
    public DecoratedForest(SimpleFD fd)
    {
        this.parents = new LinkedHashMap<>();
        this.sizes = new HashMap<>();
        this.determinants = new HashMap<>();
        this.invDeterminants = new HashMap<>();
        this.children = new HashMap<>();
        this.fd = fd;
    }

    /**
     * Returns true if this object is already in the forest
     * @param o
     * @return 
     */
    public boolean contains(Long o)
    {
        return parents.get(o) != null;
    }

    public Map<Long, Long> getParents()
    {
        return parents;
    }

    public Map<Long, Integer> getSizes()
    {
        return sizes;
    }
    
    /**
     * Finds the representative object for the set in which the object resides.
     * This implementation is based on the path halving algorithm by Tarjan
     * and Van Leeuwen [1]. See also:
     * 
     * https://en.wikipedia.org/wiki/Disjoint-set_data_structure#Finding_set_representatives
     * 
     * [1] Tarjan, Robert E.; van Leeuwen, Jan (1984).
     * "Worst-case analysis of set union algorithms".
     * Journal of the ACM. 31 (2): 245-281.
     * doi:10.1145/62.2160
     * 
     * @param o
     * @return 
     */
    public Long find(Long o)
    {
        Long representative = o;
        while(!parents.get(representative).equals(representative))
        {
            Long parent = parents.get(representative);
            Long grandParent = parents.get(parent);
            
            //Parent of representative is replaced with grand parent
            parents.put(representative,grandParent);
            children.get(grandParent).add(representative);
            children.get(parent).remove(representative);
            
            representative = parents.get(representative);
        }
        
        return representative;
    }
    
    /**
     * Merges the sets in which two given objects o1 and o2 reside. In this implementation,
     * it is first verified if both objects are present. It this is not the case,
     * they are added first.
     * 
     * @param id1
     * @param id2
     */
    private Long merge(Long id1, Long id2)
    {
        //Check if o1 is present and if not, add it.
        if(!contains(id1) || ! contains(id2))
            throw new RuntimeException("Attempt to merge identifiers "
                + id1
                + " and "
                + id2
                + " failed because one of them is not present.");
            
        //Get roots
        Long x = find(id1);
        Long y = find(id2);
        
        //Check if objects are already in the same set
        if(x.equals(y))
            return x;

        int sizeX = sizes.get(x),sizeY=sizes.get(y);
        
        //The node with more descendants becomes the parent.
        //If the two nodes have the same number of descendants, then either one can become the parent.
        //In both cases, the size of the new parent node is set to its new total number of descendants.
                
        if (sizeX >= sizeY)
        {
            //x becomes the new root
            parents.put(y, x);
            children.get(x).add(y);
            sizes.put(x, sizeX + sizeY);
            
            return x;
        }
        else
        {
            //y becomes the new root
            parents.put(x, y);
            children.get(y).add(x);
            sizes.put(y, sizeX + sizeY);
            
            return y;
        }
    }
    
    public void add(Long id, DataObject o)
    {
        if(contains(id))
            throw new RuntimeException("Cannot add id " + id + " to forest. Cause: id is already present.");
        
        //If this determinant is currently unknown, we make a class for it
        if(determinants.get(o) == null)
        {
            parents.put(id, id);
            children.put(id, new HashSet<>());
            sizes.put(id, 1);
            determinants.put(o, id);
            invDeterminants.put(id, o);
        }
        //Else we must merge classes
        else
        {
            Long currentRepresentative = determinants.get(o);
            parents.put(id, id);
            children.put(id, new HashSet<>());
            sizes.put(id, 1);
            Long newRepresentative = merge(id, currentRepresentative);
            
            determinants.put(o, newRepresentative);
            invDeterminants.remove(currentRepresentative);
            invDeterminants.put(newRepresentative, o);
        }
    }

    /**
     * Returns true if two ids are in the same set and are thus equivalent
     * under the partition of the object space.
     * @param id1
     * @param id2
     * @return 
     */
    public boolean equivalent(Long id1, Long id2)
    {
        return find(id1).equals(find(id2));
    }
    
    /**
     * Get number of partition classes.
     * @return 
     */
    public int getNumberOfPartitionClasses()
    {
        return determinants.size();
    }
    
    /**
     * This operation removes object id from its current class and moves
     * it to the class with feature o
     * @param id
     * @param o 
     */
    public void move(Long id, DataObject o)
    {
        //Remove id from it's current class
        delete(id);
        
        //Add id with new representative o
        add(id, o);
    }
    
    private void delete(Long id)
    {
        //If id is a leaf node
        if(sizes.get(id) == 1)
        {
            //Is the id is a singleton class?
            if(Objects.equals(parents.get(id), id))
            {
                DataObject o = invDeterminants.get(id);
                
                invDeterminants.remove(id);
                determinants.remove(o);
            }
            parents.remove(id);
            children.remove(id);
        }
        else
        {
            Set<Long> currentChildren = children.get(id);
            Long currentParent = parents.get(id);
            Integer currentSize = sizes.get(id);
            
            //Pick a child
            Long child = currentChildren
                .stream()
                .findFirst()
                .get();
            
            //Swap
            children.put(id, children.get(child));
            parents.put(id, child);
            sizes.put(id, sizes.get(child));
            
            children.put(child, currentChildren);
            parents.put(child, currentParent);
            sizes.put(child, currentSize - 1); //We already account for the leaf that eventually will be deleted
            
            delete(id);
        }
    }
    
    public static Collection<Set<Long>> create(Collection<DecoratedForest> forests)
    {
        throw new UnsupportedOperationException("No current implementation...");
    }
}
