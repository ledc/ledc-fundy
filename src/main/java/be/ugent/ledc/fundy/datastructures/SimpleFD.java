package be.ugent.ledc.fundy.datastructures;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * A class that represents a simplified functional dependency where the RHS
 * is constrained to singleton sets of attributes.
 * @author abronsel
 */
public class SimpleFD extends FD
{   
    private final String rhsAttribute;

    public SimpleFD(Set<String> leftHandSide, String rhsAttribute)
    {
        super(leftHandSide, Stream.of(rhsAttribute).collect(Collectors.toSet()));
        this.rhsAttribute = rhsAttribute;
    }

    public String getRhsAttribute()
    {
        return rhsAttribute;
    }
}
