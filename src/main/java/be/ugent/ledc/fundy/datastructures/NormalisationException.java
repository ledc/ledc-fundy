package be.ugent.ledc.fundy.datastructures;

import be.ugent.ledc.core.LedcException;

public class NormalisationException extends LedcException
{
    public NormalisationException()
    {
    }

    public NormalisationException(String message)
    {
        super(message);
    }

    public NormalisationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public NormalisationException(Throwable cause)
    {
        super(cause);
    }

    public NormalisationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
