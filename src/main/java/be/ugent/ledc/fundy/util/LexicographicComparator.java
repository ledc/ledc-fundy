package be.ugent.ledc.fundy.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class LexicographicComparator<T extends Comparable<T>> implements Comparator<List<T>>
{
    private final Comparator<T> elementComparator;

    public LexicographicComparator(final Comparator<T> elementComparator)
    {
        this.elementComparator = elementComparator;
    }

    public LexicographicComparator()
    {
        this((o1,o2) -> o1.compareTo(o2));
    }

    @Override
    public int compare(List<T> l1, List<T> l2)
    {
        final Iterator<T> i1 = l1.iterator(), i2 = l2.iterator();
        
        while (i1.hasNext() && i2.hasNext())
        {
            final int cmp = elementComparator.compare(i1.next(), i2.next());
            if (cmp != 0)
            {
                return cmp;
            }
        }
        if (i1.hasNext() && !i2.hasNext())
        {
            return 1;
        }
        if (!i1.hasNext() && i2.hasNext())
        {
            return -1;
        }
        return 0;
    }
}
