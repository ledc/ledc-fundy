package be.ugent.ledc.fundy.io;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.datastructures.FD;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

public class ConstraintIo
{
    public static final String IMPLICATION      = "->";
    public static final String ORDERS           = "orders";
    public static final String REVERSES         = "reverses";
    public static final String SEPARATOR        = ",";
    public static final String COMMENT_PREFIX   = "--";
    
    public static void writeRuleset(RuleSet<Dataset, FD> ruleset, File file, Charset charset) throws FileNotFoundException
    {
        writeRuleset(ruleset, file, charset, false);
    }
    
    public static void writeRuleset(RuleSet<Dataset, FD> ruleset, File file, Charset charset, boolean append) throws FileNotFoundException
    {
        try (
            PrintWriter writer = new PrintWriter(
                new OutputStreamWriter(
                        new FileOutputStream(file, append),
                        charset))
        )
        {
            ruleset
                    .stream()
                    .map(fd -> fd.toString())
                    .forEach(writer::println);
            
            writer.flush();
        }
    }
    
    public static void writeRuleset(RuleSet<Dataset, FD> ruleset, File file) throws FileNotFoundException
    {
        ConstraintIo.writeRuleset(ruleset, file, StandardCharsets.UTF_8);
    }
    
    public static RuleSet<Dataset, FD> readRuleset(File file) throws FileNotFoundException, ParseException
    {
        return readRuleset(file, StandardCharsets.UTF_8);
    }
    
    public static RuleSet<Dataset, FD> readRuleset(File file, Charset charset) throws FileNotFoundException, ParseException {

        Scanner scanner = new Scanner(file, charset.name());

        List<String> lines = new ArrayList<>();
        
        while (scanner.hasNextLine())
        {
            //Read the line, ignore leading and trailing spaces
            lines.add(scanner.nextLine().trim());
        }

        return FDParser.parseRuleset(lines);

    }
}
