package be.ugent.ledc.fundy.io;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.rules.RuleSet;
import be.ugent.ledc.fundy.datastructures.FD;
import be.ugent.ledc.fundy.datastructures.SimpleFD;
import static be.ugent.ledc.fundy.io.ConstraintIo.COMMENT_PREFIX;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FDParser
{
    public static FD parseFD(String line) throws ParseException
    {
        String[] parts = line.trim().split(ConstraintIo.IMPLICATION);
        
        if(parts.length != 2)
            throw new ParseException("Could not parse FD " + line);

        return new FD(
            parseAsSet(parts[0].trim()), //Parse LHS
            parseAsSet(parts[1].trim())  //Parse RHS
        );
    }
    
    public static SimpleFD parseSimpleFD(String line) throws ParseException
    {
        String[] parts = line.trim().split(ConstraintIo.IMPLICATION);
        
        if(parts.length != 2)
            throw new ParseException("Could not parse FD " + line);

        
        return new SimpleFD(
            parseAsSet(parts[0].trim()), //Parse LHS
            parseAsAttribute(parts[1].trim())  //Parse RHS
        );
    }
    
    private static Set<String> parseAsSet(String part) throws ParseException
    {
        if(part == null || part.isBlank())
            return new HashSet<>();
        
        Set<String> attrs = Stream
            .of(part.split(ConstraintIo.SEPARATOR,-1))
            .map(String::trim)
            .collect(Collectors.toSet());
        
        if(attrs.stream().anyMatch(String::isBlank))
            throw new ParseException("Cannot parse FD. Cause: empty attribute detected.");
        
        return attrs;
    }
    
    private static String parseAsAttribute(String part) throws ParseException
    {
        if(part.contains(ConstraintIo.SEPARATOR))
            throw new ParseException("Cannot parse RHS as single attribute. "
                + "Cause: '" 
                + part 
                + "' contains " 
                + ConstraintIo.SEPARATOR);
        
        return part.trim();
    }
    
    public static RuleSet<Dataset, FD> parseRuleset(List<String> lines) throws ParseException
    {
        Set<FD> rules = new HashSet<>();
        
        for (String line: lines)
        {
            //Comments are ignored
            if (line.startsWith(COMMENT_PREFIX) || line.isEmpty())
                continue;

            rules.add(FDParser.parseFD(line));
        }

        return new RuleSet<>(rules);
    }

}
