# ledc-fundy

## Why

Functional dependencies (FDs) are an essential part of the relational model for databases.
They define the notion of (candidate) *keys*, lie at the basis of *normalization* through Heath's theorem and act as an effective *data quality constraint* to ensure data consistency.


## What
This repository is part of ledc, which is short for *Lightweight Engine for Data quality Control*. 
It is a modular framework designed for data quality monitoring and improvement in several different ways.

The current repo contains ledc-fundy: a small and simple toolbox for working with FDs.
It provides several algorithms for different problems concerning FDs.
The main features of ledc-fundy are

* **Reasoning**: an important aspect of FDs is reasoning about them. Ledc-fundy provides algorithms for computation of attribute closure, candidate keys, minimal covers and implication of FDs.

* **Discovery**: in several cases, it is useful to know all minimal and non-trivial FDs that are satisfied.
ledc-fundy implements the well-known TANE algorithm to find all such FDs. 

* **Normalization**: in order to analyze a database schema, determining the normal form is an important step.
ledc-fundy allows verification of normal forms up to Boyce-Codd Normal Form (BCNF) and provides two algorithms for normalization: 3NF synthesis and BCNF decomposition.

* **Repair**: when some FDs are violated in a relation, finding a clean variant of that relation is known as *repairing*.
ledc-fundy offers a very fast algorithm known as the Swipe algorithm to produce such repairs.

## Getting started

As all current and future components of ledc, ledc-fundy is written in Java.
To run it, as of version 1.2, you require Java 17 (or higher) and Maven.
Just pull the code from this repository and build it with Maven.
Alternatively, you can have a look at the registry to download an executable JAR file.

## References

The repair algorithm for FDs is described in the following publications.

* Toon Boeckling and Antoon Bronselaer, [Cleaning Data with Swipe](https://arxiv.org/abs/2403.19378), arXiv preprint, (**2024**)
